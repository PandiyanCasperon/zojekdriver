package com.cabilyhandyforalldinedoo.chd.viewmodelfortriplist

import android.app.Activity
import android.content.Context
import android.location.Geocoder
import com.cabilyhandyforalldinedoo.chd.Modal.ridelistsummaryModel
import com.cabilyhandyforalldinedoo.chd.commonutils.CurrencySymbolConventer
import io.realm.Realm
import org.json.JSONArray
import org.json.JSONObject

import java.util.*


class ridedetailrepostiatry(private val listener: ridedetailspageListener)
{

    val realm by lazy { Realm.getDefaultInstance() }
    lateinit var currencySymbolConventer: CurrencySymbolConventer



    fun serachrideiddata(mContext: Context, rideid:String)
    {
        val results = realm.where(tripdetailsrealmm::class.java).equalTo("ride_id", rideid).findAll()
        for (recentfields in results)
        {

            val driver_name = recentfields.driver_name.toString()
            val driver_image = recentfields.driver_image.toString()
            val vehile_maker_model = recentfields.category.toString()
            val vehicle_number = recentfields.vehicle_number.toString()
            val avg_review =recentfields.avg_review.toString()
            val category =recentfields.category.toString()

            listener!!.drivername(driver_name)
            listener!!.driverimage(driver_image)
            listener!!.driverrating(avg_review)
            listener!!.drivervehicletype(category)
            listener!!.drivervehiclenumber(vehicle_number)

            val cancellationreason = recentfields.cancelreason.toString()

            val pickup_address_txt1 =recentfields.pickuplocation.toString()
            val drop_address_txt1 =recentfields.droplocation.toString()
            val payment_method =recentfields.payment_typeuser.toString()
            val picktime =recentfields.pickup_time.toString()
            var droptimed =recentfields.drop_time.toString()
            val grand_fare =recentfields.grand_fare.toString()
            val invoice_src =recentfields.invoice_src.toString()

            listener!!.pickup_address_txt(pickup_address_txt1)
            listener!!.drop_address_txt(drop_address_txt1)





            if(droptimed.equals(""))
            {
                val cancelled_time =recentfields.cancelled_time.toString()
                droptimed = cancelled_time
            }

            listener!!.cancelreason(cancellationreason)
            listener!!.pickuptime("0.00")
            listener!!.droptime("0.00")

            val units =recentfields.units.toString()



            listener!!.ridekm("0.00"+" "+"KM")


            val currency = units
            currencySymbolConventer = CurrencySymbolConventer()

            var currency_symbol = currencySymbolConventer.getCurrencySymbol(currency)

            val driverrevenue =recentfields.payment_method.toString()
            if(!driverrevenue.equals(""))
            {
                listener!!.fundriverrevenue(currency_symbol+driverrevenue)
            }

            listener!!.paidamount(grand_fare)

            listener!!.symbl(currency_symbol)
            listener!!.paymentmethod(payment_method)

            val fare_summary =recentfields.fare_summary.toString()
            val farelist = ArrayList<ridelistsummaryModel>()
            val fare_summaryarray = JSONArray(fare_summary)

            for (j in 0 until fare_summaryarray.length())
            {
                val job = fare_summaryarray.getJSONObject(j)
                val title = job.getString("title").toString()
                val value = job.getString("value").toString()
                farelist.add(ridelistsummaryModel(1, title, currency_symbol+value))
            }
            listener!!.farebreakup(farelist)


            if(!invoice_src.equals(""))
            {
                listener!!.mapimagesave(invoice_src)
            }
        }
    }
}