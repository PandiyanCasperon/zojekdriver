package com.cabilyhandyforalldinedoo.chd.retrofit

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object RetrofitInstance {



  private const val url = "https://zojek.casperon.co/"
  private const val baseUrl = url + "driver/v1/"
  private const val baseUrl2 = url + "trips/v1/"

  /*private const val url = "https://cabily-e.zoplay.com/"
  private const val baseUrl = url + "v1/"*/
  val appmoreinfo = baseUrl + "api/driver/init"
  val triplist = baseUrl2 + "api/trip/driver/get"
  val verifymobile = baseUrl + "api/driver/signup/vcode/get"
  val updatemobileno = baseUrl + "api/driver/update/mobile"
  val updateemailid = baseUrl + "api/driver/update/email"
  val resendotp = baseUrl + "api/driver/signup/vcode/get"
  val checkuserexist = baseUrl + "api/driver/login"
  val onlineofflinecall = baseUrl + "api/driver/workstatus/update"
  val updatedriverlat = baseUrl + "api/driver/location/update"
  val dashboardcallapi = baseUrl + "api/driver/dashboard/get"
  val servicelocationapi = baseUrl + "api/driver/loccat/get"
  val getdocument = baseUrl + "api/driver/document/get"
  val getcatmakemodel = baseUrl + "api/driver/vehicleinfo/get"
  val getvehicleid = baseUrl + "api/driver/vehicle/update"
  val fianalmovepage = baseUrl + "api/driver/stages/update"
  val documentsubmit = baseUrl + "api/driver/documents/update"
  val logoutcall = baseUrl + "api/driver/logout"
  val basicregister = baseUrl + "api/driver/signup/submit"
  val editprofile = baseUrl + "api/driver/update/profile"

  val ridedetailss = baseUrl2 + "api/trip/driver/track/info"
  val acknowledgecallhit = baseUrl2 + "api/trip/request/ack/update"
  val cancelreason = baseUrl2 + "api/trip/driver/cancel/reason"
  val cancelthisride = baseUrl2 + "api/trip/driver/cancel/submit"
  val tripupdatearrived = baseUrl2 + "api/trip/location/arrived"
  val begintripurl = baseUrl2 + "api/trip/begin"
  val endtripurl = baseUrl2 + "api/trip/destination/reached"
  val receivecash = baseUrl2 + "api/trip/collect/cash"
  val submitrating = baseUrl2 + "api/trip/driver/rating/submit"
  val getvehiclelistt = baseUrl + "api/driver/vehicle/get"
  val defaultvechicleupdate = baseUrl + "api/driver/vehicle/default"
  val editvehicleinfo = baseUrl + "api/driver/vehicle/info"
  val driverdocinfo = baseUrl + "api/driver/info"

  // Start of end points
  val verifyemail = baseUrl + "api/driver/registration/verify/email"
  val nearingcallhit = baseUrl + "api/send-user-alert"


  val skiprideid = baseUrl + "api/reviews/skip"


  val chaturl = url + "v7/app/chat/push-chat-message"
  val tripdetailss = baseUrl + "api/driver/trip/list"
  val tripdetailssfull = baseUrl + "api/driver/trip/view"

  // End of end points





    private var retrofit: Retrofit? = null
    private var retrofitGoogle: Retrofit? = null
    private val okHttpClient = OkHttpClient.Builder().connectTimeout(90, TimeUnit.SECONDS)
            .build()


    fun getInstance(): RetrofitInterface {
        if (retrofit == null)
            retrofit = Retrofit.Builder()
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(baseUrl)
                    .build()
        return retrofit!!.create(RetrofitInterface::class.java)
    }

    fun getGoogleInstance(): RetrofitInterface {
        if (retrofitGoogle == null)
            retrofitGoogle = Retrofit.Builder()
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl("https://maps.googleapis.com/")
                    .build()
        return retrofitGoogle!!.create(RetrofitInterface::class.java)
    }


}
