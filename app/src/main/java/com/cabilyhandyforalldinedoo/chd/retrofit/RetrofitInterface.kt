package com.cabilyhandyforalldinedoo.chd.retrofit

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*


interface RetrofitInterface
{
    @FormUrlEncoded
    @POST("api/driver/registration/verify/mobile")
    fun getlogindetails(@HeaderMap header: HashMap<String, String>, @FieldMap params: HashMap<String, String>): Call<ResponseBody>

    @GET
    fun getRouteList(@Url url: String): Call<ResponseBody>


    @FormUrlEncoded
    @POST("api/user/otp/resend")
    fun resendotp(@HeaderMap header: HashMap<String, String>, @FieldMap params: HashMap<String, String>): Call<ResponseBody>


    @FormUrlEncoded
    @POST("api/get-app-info")
    fun getappinfodetails(@HeaderMap header: HashMap<String, String>, @FieldMap params: HashMap<String, String>): Call<ResponseBody>


    @FormUrlEncoded
    @POST("api/driver/registration/personal")
    fun sendprofiledetails(@HeaderMap header: HashMap<String, String>, @FieldMap params: HashMap<String, String>): Call<ResponseBody>


    @FormUrlEncoded
    @POST("api/driver/registration/vehicles/get")
    fun categoryfetchapi(@HeaderMap header: HashMap<String, String>, @FieldMap params: HashMap<String, String>): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api/driver/registration/init")
    fun locationfetchapi(@HeaderMap header: HashMap<String, String>, @FieldMap params: HashMap<String, String>): Call<ResponseBody>



    @FormUrlEncoded
    @POST("api/driver/registration/update/vehicle")
    fun savestep1(@HeaderMap header: HashMap<String, String>, @FieldMap params: HashMap<String, String>): Call<ResponseBody>


    @FormUrlEncoded
    @POST("api/driver/dashboard")
    fun driverdashboardapi(@HeaderMap header: HashMap<String, String>, @FieldMap params: HashMap<String, String>): Call<ResponseBody>


    @FormUrlEncoded
    @POST("api/driver/registration/document/get")
    fun getdocument(@HeaderMap header: HashMap<String, String>, @FieldMap params: HashMap<String, String>): Call<ResponseBody>


    @FormUrlEncoded
    @POST("api/driver/registration/document/upload")
    fun imageuploadpai(@HeaderMap header: HashMap<String, String>, @FieldMap params: HashMap<String, String>): Call<ResponseBody>



    @FormUrlEncoded
    @POST("api/driver/registration/document/update")
    fun finaldocumenttwosend(@HeaderMap header: HashMap<String, String>, @FieldMap params: HashMap<String, String>): Call<ResponseBody>



    @FormUrlEncoded
    @POST("api/driver/document/verification")
    fun verficationcall(@HeaderMap header: HashMap<String, String>, @FieldMap params: HashMap<String, String>): Call<ResponseBody>


    @FormUrlEncoded
    @POST("api/driver/update/document")
    fun reuploadimage(@HeaderMap header: HashMap<String, String>, @FieldMap params: HashMap<String, String>): Call<ResponseBody>



    @FormUrlEncoded
    @POST("api/location/update/driver")
    fun onlinecallupdate(@HeaderMap header: HashMap<String, String>, @FieldMap params: HashMap<String, String>): Call<ResponseBody>



    @FormUrlEncoded
    @POST("api/driver/update/availability")
    fun driveravailable(@HeaderMap header: HashMap<String, String>, @FieldMap params: HashMap<String, String>): Call<ResponseBody>


    @FormUrlEncoded
    @POST("api/trip/request/ack")
    fun ackapi(@HeaderMap header: HashMap<String, String>, @FieldMap params: HashMap<String, String>): Call<ResponseBody>



    @FormUrlEncoded
    @POST("api/booking/accept")
    fun acceptapi(@HeaderMap header: HashMap<String, String>, @FieldMap params: HashMap<String, String>): Call<ResponseBody>

}


