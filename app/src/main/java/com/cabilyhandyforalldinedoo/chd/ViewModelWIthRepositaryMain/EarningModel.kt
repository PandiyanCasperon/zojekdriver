package com.cabilyhandyforalldinedoo.chd.ViewModelWIthRepositaryMain

data class EarningModel(
    val symbol: String,
    val amount: String,
    val triptype: String,
    val lastat: String,
    val categories: String
)