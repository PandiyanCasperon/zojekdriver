package com.cabilyhandyforalldinedoo.chd.ViewModelWIthRepositaryMain;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;

import com.cabilyhandyforalldinedoo.chd.Backgroundservices.updatelocationtoserverapi;
import com.cabilyhandyforalldinedoo.chd.EventBusConnection.IntentServiceResult;
import com.cabilyhandyforalldinedoo.chd.SessionManagerPackage.SessionManager;
import com.cabilyhandyforalldinedoo.chd.commonutils.AppUtils;

import org.greenrobot.eventbus.EventBus;

import java.util.TimerTask;

import cabily.handyforall.dinedoo.R;

public class CustomTimerTask extends TimerTask {

    SessionManager mSessionManager = null;
    private Context context;
    private Handler mHandler = new Handler();

    public CustomTimerTask(Context con)
    {
        this.context = con;
    }

    @Override
    public void run() {
        new Thread(new Runnable() {

            public void run() {

                mHandler.post(new Runnable()
                {
                    public void run()
                    {
                        updatelocationtoservice();
                    }
                });
            }
        }).start();

    }


    private void updatelocationtoservice()
    {
        EventBus.getDefault().post(new IntentServiceResult(Activity.RESULT_OK, "loadme", "loadme"));
        Log.e("updating data to server",
                "update--");
        //Toast.makeText(context,"updated",Toast.LENGTH_SHORT).show();
        mSessionManager = new SessionManager(context);
        Intent intent = new Intent(context, updatelocationtoserverapi.class);
        intent.putExtra(context.getString(R.string.intent_putextra_api_key), context.getString(R.string.onlinestatuysupdate));
        intent.putExtra("onlinelatitude", mSessionManager.getOnlineLatitiude());
        intent.putExtra("onlinelongitude",mSessionManager.getOnlineLongitude());
        intent.putExtra("onlinebearing",mSessionManager.getOnlineBEaring());
        context.startService(intent);
    }

}
