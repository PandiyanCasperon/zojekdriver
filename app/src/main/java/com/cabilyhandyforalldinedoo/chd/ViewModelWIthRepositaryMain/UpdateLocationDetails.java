package com.cabilyhandyforalldinedoo.chd.ViewModelWIthRepositaryMain;


import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.cabilyhandyforalldinedoo.chd.xmpp.RoosterConnection;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;


/**
 * location update service continues to running and getting location information
 */
public class UpdateLocationDetails extends JobService {
    public static RoosterConnection.ConnectionState sConnectionState;
    private boolean mActive;
    private Thread mThread;
    private Handler mTHandler;
    private RoosterConnection mConnection;
    Timer timer = new Timer();
    TimerTask updateProfile = new CustomTimerTask(UpdateLocationDetails.this);
    public UpdateLocationDetails() {
    }
    public static RoosterConnection.ConnectionState getState()
    {
        if (sConnectionState == null)  return RoosterConnection.ConnectionState.DISCONNECTED;
        return sConnectionState;
    }
    @Override
    public boolean onStartJob(JobParameters params) {
        return true;
    }
    @Override
    public boolean onStopJob(JobParameters params)
    {
        return false;
    }
    @Override
    public void onCreate()
    {
        timer.scheduleAtFixedRate(updateProfile, 0, 10000);
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        start();
        return START_STICKY;
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }
    @Override
    public void onRebind(Intent intent) {
        super.onRebind(intent);
    }
    @Override
    public boolean onUnbind(Intent intent)
    {
        return true;
    }
    private void initConnection()
    {
        if( mConnection == null) mConnection = new RoosterConnection(this);
        try
        {
            mConnection.connect();
        }
        catch (IOException | SmackException | XMPPException e)
        {
            stopSelf();
        }
    }
    public void start()
    {
        if(!mActive)
        {
            mActive = true;
            if( mThread ==null || !mThread.isAlive())
            {
                mThread = new Thread(new Runnable() {
                    @Override
                    public void run() {

                        Looper.prepare();
                        mTHandler = new Handler();
                        initConnection();
                        Looper.loop();
                    }
                });
                mThread.start();
            }
        }
    }
    public void stop()
    {
        mActive = false;
        mTHandler.post(new Runnable() {
            @Override
            public void run() {
                if( mConnection != null)  mConnection.disconnect();
            }
        });
    }
    @Override
    public void onDestroy()
    {
        timer.cancel();
        stop();
    }
}
