package com.cabilyhandyforalldinedoo.chd.ViewModelWIthRepositaryMain


import java.util.ArrayList

interface Listener {
    fun onDataReceived(mutableLiveData: ArrayList<MianPageDataModel>)
    fun onOnlinestatusChange(onlinesttau: String)
    fun onError(error: Int)
    fun onAmount(amount: String)
    fun onEarning(earning: String)
    fun onEarningsAdded(mutableLiveData: List<EarningModel>)
    fun onDocstatus(mutableLiveData:String)
    fun onDutyride(mutableLiveData:String)
    fun onMobileNo(mobileno: String)
}