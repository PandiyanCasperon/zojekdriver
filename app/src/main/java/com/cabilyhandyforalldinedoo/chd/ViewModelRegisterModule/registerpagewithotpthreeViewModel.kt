package com.cabilyhandyforalldinedoo.chd.ViewModelRegisterModule


import android.content.Context
import android.content.Intent
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import cabily.handyforall.dinedoo.R
import com.cabilyhandyforalldinedoo.chd.Backgroundservices.commonapifetchservice
import com.cabilyhandyforalldinedoo.chd.SessionManagerPackage.SessionManager
import com.cabilyhandyforalldinedoo.chd.data.chatdb.DbHelper
import com.cabilyhandyforalldinedoo.chd.data.onridelatandlong.onrideDbHelper
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*


class registerpagewithotpthreeViewModel : ViewModel()
{
    private val countrycodestringemit = MutableLiveData<String>()
    private var mSessionManager: SessionManager? = null
    private val otpwithstatus = MutableLiveData<String>()
    private val movetonextpage = MutableLiveData<Int>()
    private val closeactvitiy = MutableLiveData<Int>()
    private val resendotp = MutableLiveData<Int>()

    private val decdeclasstosend = MutableLiveData<Int>()

    fun countrycodeobservervalue(): MutableLiveData<String>
    {
        return countrycodestringemit
    }
    fun otpwithstatusobservervalue(): MutableLiveData<String>
    {
        return otpwithstatus
    }
    fun decideclassobserver(): MutableLiveData<Int>
    {
        return decdeclasstosend
    }
    fun getflagdetailsfromsession(mContext: Context)
    {
        mSessionManager = SessionManager(mContext)
        var countrycode= mSessionManager?.getFlagDetails()!![mSessionManager!!.countrycodesaved]!!
        countrycodestringemit.value = countrycode
    }

    fun clearchatrecord(mContext:Context)
    {
        mContext.deleteDatabase(DbHelper.DATABASE_NAME)
        mContext.deleteDatabase(onrideDbHelper.DATABASE_NAME)
    }
    fun movetonextpageobserver(): MutableLiveData<Int>
    {
        return movetonextpage
    }
    fun closescreenobserver(): MutableLiveData<Int>
    {
        return closeactvitiy
    }

    fun resendobserver(): MutableLiveData<Int>
    {
        return resendotp
    }



    fun closepage()
    {
        closeactvitiy.value = 1
    }

    fun resendotpcall()
    {
        resendotp.value = 1
    }

    fun splitresponse(finalresponse:String)
    {
        try
        {
            val response_json_object = JSONObject(finalresponse)
            try
            {
                val status = response_json_object.getString("status")
                if (status.equals("1"))
                {
                    val otp = response_json_object.getString("otp")
                    val otp_status = response_json_object.getString("otp_status")
                    otpwithstatus.value=otp+","+otp_status
                }
            }
            catch (e: Exception)
            {
            }
        }
        catch (e: Exception)
        {

        }
    }


    fun splitexistingcall(finalresponse:String)
    {
        try
        {
            val response_json_object = JSONObject(finalresponse)
            try
            {
                val status = response_json_object.getString("status")
                if (status.equals("1"))
                {
                    val response = response_json_object.getString("response")
                    val response_json_object = JSONObject(response)

                    val driver_id = response_json_object.getString("driver_id")
                    val firstname = response_json_object.getString("first_name")
                    val last_name = response_json_object.getString("last_name")
                    val email = response_json_object.getString("email")
                    val gender = response_json_object.getString("gender")
                    val dob = response_json_object.getString("dob")
                    val driver_image = response_json_object.getString("image")
                    val dial_code = response_json_object.getString("dial_code")
                    val mobile_number = response_json_object.getString("mobile_number")
                    val address = response_json_object.getString("address")
                    val country = response_json_object.getString("country")
                    val state = response_json_object.getString("state")
                    val city = response_json_object.getString("city")
                    val zipcode = response_json_object.getString("zipcode")


                    var spf = SimpleDateFormat("yyyy-MM-dd")
                    val newDate: Date = spf.parse(dob)
                    spf = SimpleDateFormat("dd/MM/yyyy")
                    var date = spf.format(newDate)

                    mSessionManager!!.createLoginSession(driver_id,firstname,last_name,firstname,email,gender,date,driver_image,"2",dial_code,mobile_number,address,country,state,city,"",zipcode)
                    mSessionManager!!.setvehiclenumber("")
                    mSessionManager!!.setModelnamealone("")
                    mSessionManager!!.setReview("0")
                    mSessionManager!!.setDocPending("1")
                    mSessionManager!!.setCategoryNamealone("")
                    mSessionManager!!.setHasseesion(true)

                    decdeclasstosend.value = 1
                }
                else
                {
                    decdeclasstosend.value = 0
                }
            }
            catch (e: Exception)
            {
            }
        }
        catch (e: Exception)
        {

        }
    }

    fun startmobileapi(mContext: Context,mobileno: String,code:String)
    {
        val serviceClass = commonapifetchservice::class.java
        val intent = Intent(mContext, serviceClass)
        intent.putExtra(mContext.getString(R.string.intent_putextra_api_key), mContext.getString(R.string.resendotp))
        intent.putExtra("mobileno", mobileno)
        intent.putExtra("code", code)
        mContext.startService(intent)
    }

    override fun onCleared()
    {
    }
}

