package com.cabilyhandyforalldinedoo.chd.ViewModelRegisterModule


import com.cabilyhandyforalldinedoo.chd.Modal.ServiceLocationModel
import java.util.ArrayList

interface ServiceLocationListener {
    fun onDataReceived(mutableLiveData: ArrayList<ServiceLocationModel>)
    fun onError(error: Int)
}