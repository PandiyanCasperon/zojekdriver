package com.cabilyhandyforalldinedoo.chd.ui.sidemenus

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import cabily.handyforall.dinedoo.R
import cabily.handyforall.dinedoo.databinding.EarningpagetwolayoutBinding
import com.cabilyhandyforalldinedoo.chd.EventBusConnection.IntentServiceResult
import com.cabilyhandyforalldinedoo.chd.adaptersofchd.Weeklyadapter
import com.cabilyhandyforalldinedoo.chd.clickableInterface.tripdetailclick
import com.cabilyhandyforalldinedoo.chd.commonutils.AppUtils
import com.cabilyhandyforalldinedoo.chd.earningsviewmodel.WeekModel
import com.cabilyhandyforalldinedoo.chd.earningsviewmodel.viewmodelforearningstwo
import com.cabilyhandyforalldinedoo.chd.ui.registeration.LocaleAwareCompatActivity
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.util.*


class earningpagetwo : LocaleAwareCompatActivity ()
{
    var saveoverallarraylis:ArrayList<WeekModel> = ArrayList<WeekModel>()
    private lateinit var mContext: Activity
    lateinit var binding: EarningpagetwolayoutBinding
    private lateinit var viewModel: viewmodelforearningstwo
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.earningpagetwolayout)
        mContext = this@earningpagetwo
        initviewmodel()
        binding.setViewModel(viewModel)
        binding.close.setOnClickListener {
            finish()
        }

    }
    //Viewmodel Observer Part
    fun initviewmodel()
    {
        viewModel = ViewModelProviders.of(this).get(viewmodelforearningstwo::class.java)
        binding.loader.visibility = View.VISIBLE
        viewModel.retrieveapicall(mContext)
        viewModel.gotdataforlist().observe(this, Observer {
            if(it != 1)
            {
                binding.loader.visibility = View.GONE
                AppUtils.commonerrorsheet(mContext,getString(R.string.failed),getString(R.string.failedproblem))
            }
        })

        viewModel.triplistobserver().observe(this, Observer {
            saveoverallarraylis = it
                triplistview(it)
        })


    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun doThis(intentServiceResult: IntentServiceResult)
    {
        var apiName: String = intentServiceResult.apiName
        if (apiName.equals(getString(R.string.retrievetripdetail)))
        {
            var response: String = intentServiceResult.resultValue
            if (response != "failed")  viewModel.splitapiresopnse(applicationContext,response)
            else
            {
                binding.loader.visibility = View.GONE
                AppUtils.commonerrorsheet(mContext,getString(R.string.failed),getString(R.string.failedproblem))
            }
        }
    }
    override fun onResume()
    {
        super.onResume()
        if(!EventBus.getDefault().isRegistered(this))  EventBus.getDefault().register(this)
    }
    override fun onDestroy()
    {
        super.onDestroy()
        if(EventBus.getDefault().isRegistered(this))   EventBus.getDefault().unregister(this)
    }
    fun triplistview(tripdata: ArrayList<WeekModel>)
    {
        if(tripdata.size > 0)
        {

            binding.tripsrecylerview.also {
                it.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
                it.setHasFixedSize(true)
                it.adapter = Weeklyadapter(this,tripdata, object : tripdetailclick {
                    override fun onRecyclerViewItemClick(view: View, rideid: String,mapimage:String) {
                        val activityclass = fulltripdetails::class.java
                        val intent = Intent(mContext, activityclass)
                        intent.putExtra("rideid",rideid)
                        intent.putExtra("mapimage", mapimage)
                        mContext.startActivity(intent)
                    }
                })
            }
            binding.loader.visibility = View.GONE
        }
        else  AppUtils.commonerrorsheet(mContext,getString(R.string.failed),getString(R.string.nodataaviable))
    }
}