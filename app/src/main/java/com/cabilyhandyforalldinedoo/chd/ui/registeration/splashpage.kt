package com.cabilyhandyforalldinedoo.chd.ui.registeration

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import cabily.handyforall.dinedoo.R
import com.cabilyhandyforalldinedoo.chd.Backgroundservices.commonapifetchservice
import com.cabilyhandyforalldinedoo.chd.EventBusConnection.IntentServiceResult
import com.cabilyhandyforalldinedoo.chd.ViewModelRegisterModule.splashpageViewModel
import com.cabilyhandyforalldinedoo.chd.commonutils.AppUtils
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class splashpage : AppCompatActivity()
{
    private lateinit var mContext: Activity
    lateinit var mViewModel: splashpageViewModel
    private var mDelayHandler: Handler? = null
    private val SPLASH_DELAY: Long = 1000
    var hassessionn:Boolean = false
    var stepsvalue:String = ""
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splashpage)
        mContext = this@splashpage
        initViewModel()           // initlizing view model
        getappinfodetails()       // App info call hit
    }
    private fun initViewModel()
    {
        mViewModel = ViewModelProviders.of(this).get(splashpageViewModel::class.java)
        mViewModel.checkautvalue(mContext)
        mViewModel.getMainPage(mContext)
        mViewModel.hassessionobservervalue().observe(this, Observer {
           if(it == false)
               mViewModel.getLoginsession(mContext)
            else
               mViewModel.mainpage(mContext)
        })
        mViewModel.sessionobservervalue().observe(this, Observer {
            hassessionn=it
            mViewModel.getStepsValue(mContext)
        })
        mViewModel.getauthval().observe(this, Observer {
            if(!EventBus.getDefault().isRegistered(this))
                EventBus.getDefault().register(this)
        })
        mViewModel.getstepsobservervalue().observe(this, Observer {
            stepsvalue=it
            mDelayHandler = Handler()
            mDelayHandler!!.postDelayed(mRunnable, SPLASH_DELAY)
        })
    }
    // post delayed class
    internal val mRunnable: Runnable = Runnable {
        if (!isFinishing) movetonextpage()
    }
    // clearing of used operation
    public override fun onDestroy()
    {
        if (mDelayHandler != null)
        {
            mDelayHandler!!.removeCallbacks(mRunnable)
        }
        if(EventBus.getDefault().isRegistered(this))  EventBus.getDefault().unregister(this)
        super.onDestroy()
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun doThis(intentServiceResult: IntentServiceResult)
    {
        var apiName: String = intentServiceResult.apiName
        if (apiName.equals(getString(R.string.getappinfohit)))
        {
            var response: String = intentServiceResult.resultValue
            if (response == "1")  movetonextpage()
            else  AppUtils.commonerrorsheet(mContext,getString(R.string.failed),getString(R.string.failedproblem))
        }
    }
    private fun getappinfodetails()
    {
        val registerpagetwopage = Intent(applicationContext, commonapifetchservice::class.java)
        registerpagetwopage.putExtra(getString(R.string.intent_putextra_api_key), getString(R.string.getappinfohit))
        startService(registerpagetwopage)
    }
    // Decide which page to move on
    fun movetonextpage()
    {
       if (hassessionn == true)  mViewModel.documentpageone(mContext)
       else mViewModel.registerpageones(mContext)
    }
}