package com.cabilyhandyforalldinedoo.chd.ui.sidemenus

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import cabily.handyforall.dinedoo.R
import cabily.handyforall.dinedoo.databinding.EarningpagefirstBinding
import com.cabilyhandyforalldinedoo.chd.earningsviewmodel.earningsviewmodel
import com.cabilyhandyforalldinedoo.chd.ui.MainPage.settingspage
import com.cabilyhandyforalldinedoo.chd.ui.registeration.LocaleAwareCompatActivity
import kotlinx.android.synthetic.main.earningpagefirst.*
import lecho.lib.hellocharts.gesture.ZoomType
import lecho.lib.hellocharts.model.*
import lecho.lib.hellocharts.util.ChartUtils
import java.util.*

class earningspage : LocaleAwareCompatActivity()
{

    private lateinit var mContext: Activity
    lateinit var binding: EarningpagefirstBinding
    private lateinit var viewModel: earningsviewmodel

    val months = arrayOf("01", "02", "03", "04", "05", "06", "07")



    private var columnData: ColumnChartData? = null
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.earningpagefirst)


        mContext = this@earningspage
        initviewmodel()
        loader.visibility = View.VISIBLE
        generateColumnData()

        closeall.setOnClickListener{
            finish()
        }
        weeklyreport.setOnClickListener{
           weeklyreport()
        }
        weeklysummary.setOnClickListener{
            weeklysummaryreport()
        }
    }
    //Viewmodel Observer Part
    fun initviewmodel()
    {
        viewModel = ViewModelProviders.of(this).get(earningsviewmodel::class.java)
    }


    private fun generateColumnData()
    {
        val numColumns: Int = months.size
        val axisValues: MutableList<AxisValue> = ArrayList()
        var columns: MutableList<Column> = ArrayList()
        var values: MutableList<SubcolumnValue?>
        for (i in 0 until numColumns)
        {
            values = ArrayList()
            values.add(SubcolumnValue(Math.random().toFloat() * 50f + 5, ChartUtils.pickColor()))
            axisValues!!.add(AxisValue(i.toFloat()).setLabel(months.get(i)))
            columns.add(Column(values).setHasLabelsOnlyForSelected(true))
        }
        columnData = ColumnChartData(columns)
        columnData!!.setAxisXBottom(Axis(axisValues).setHasLines(true))
        columnData!!.setAxisYLeft(Axis().setHasLines(true).setMaxLabelChars(2))
        chart_bottom!!.columnChartData = columnData
        chart_bottom.isValueSelectionEnabled = true
        chart_bottom.zoomType = ZoomType.HORIZONTAL

        // Start new data animation with 300ms duration;
        chart_bottom.startDataAnimation(300)

    }

    fun weeklyreport()
    {
        val intent_otppage = Intent(mContext, earningpagetwo::class.java)
        startActivity(intent_otppage)
    }

    fun weeklysummaryreport()
    {
        val intent_otppage = Intent(mContext, earningpagethree::class.java)
        startActivity(intent_otppage)
    }

}