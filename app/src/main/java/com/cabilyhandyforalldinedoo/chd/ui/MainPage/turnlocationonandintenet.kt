package com.cabilyhandyforalldinedoo.chd.ui.MainPage

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.LocationManager
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import cabily.handyforall.dinedoo.R
import com.cabilyhandyforalldinedoo.chd.SessionManagerPackage.SessionManager
import com.cabilyhandyforalldinedoo.chd.ViewModelWIthRepositaryMain.MainViewModel
import com.cabilyhandyforalldinedoo.chd.mylocation.AppConstants
import com.cabilyhandyforalldinedoo.chd.mylocation.GpsUtils
import com.cabilyhandyforalldinedoo.chd.ui.registeration.LocaleAwareCompatActivity
import kotlinx.android.synthetic.main.locationonoffmissing.*
import pub.devrel.easypermissions.EasyPermissions


class turnlocationonandintenet : LocaleAwareCompatActivity()
{

    private lateinit var mContext: Activity
    private lateinit var viewModel: MainViewModel
    lateinit var mSessionManager: SessionManager
    private var isGPS:Boolean = false;
    private val LOCATION_AND_CONTACTS = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION)
    private var RC_LOCATION_CONTACTS_PERM:Int = 124

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.locationandinteneton)
        mContext = this@turnlocationonandintenet
        mSessionManager = SessionManager(mContext!!)
        initviewmodel()
        //click opertaions
        backbutton.setOnClickListener {
            finish()
        }
        btn_done.setOnClickListener {
            locationAndContactsTask()
        }
        btn_donelayout.setOnClickListener {
            locationAndContactsTask()
        }
    }

    fun initviewmodel()
    {
      viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)

    }
    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>,
                                            grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == 124)
        {
            for (grantResult in grantResults) {
                if (grantResult == PackageManager.PERMISSION_GRANTED)
                {
                    locationAndContactsTask()
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK)
        {
            if (requestCode == AppConstants.GPS_REQUEST)
            {
                isGPS = true; // flag maintain before get location
                gpsenabledfunction()
            }
            else
            {
               Toast.makeText(applicationContext,"failed",Toast.LENGTH_LONG).show()
            }
        }
    }

    fun gpsenabledfunction()
    {
        if(mSessionManager.ridehasSession() == true)
        {
            val mainpage = Intent(mContext, requestmianscreen::class.java)
            mainpage.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(mainpage)
            finish()
        }
        else
        {
            val mainpage = Intent(mContext, mianscreen::class.java)
            mainpage.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(mainpage)
            finish()
        }
    }


    fun locationAndContactsTask()
    {
        if (hasLocationAndContactsPermissions())
        {
            // Permission given
            checklocationonedindevice()
        } else {
            // Ask for both permissions
            EasyPermissions.requestPermissions(this,"",RC_LOCATION_CONTACTS_PERM,*LOCATION_AND_CONTACTS)
        }
    }

    private fun hasLocationAndContactsPermissions(): Boolean {
        return EasyPermissions.hasPermissions(this, *LOCATION_AND_CONTACTS)
    }

    fun checklocationonedindevice()
    {
        try
        {
            val lm = applicationContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager;
            var gps_enabled:Boolean = false;
            var network_enabled:Boolean = false;
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER)
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
            if(!gps_enabled && !network_enabled)
            {
                GpsUtils(this).turnGPSOn( GpsUtils.onGpsListener() {
                    @Override
                    fun gpsStatus( isGPSEnable:Boolean)
                    {
                        isGPS = isGPSEnable;
                    }
                })
            }
            else
            {
                gpsenabledfunction()
            }
        }
        catch (e: Exception)
        {
        }
    }

}