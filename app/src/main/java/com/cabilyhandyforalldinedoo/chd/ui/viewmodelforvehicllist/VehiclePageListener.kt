package com.cabilyhandyforalldinedoo.chd.ui.viewmodelforvehicllist

import com.cabilyhandyforalldinedoo.chd.Modal.DefaultVehicleListModel
import com.cabilyhandyforalldinedoo.chd.Modal.VehicleListModel
import com.cabilyhandyforalldinedoo.chd.Modal.vehicleinfoedit


interface VehiclePageListener {

    fun errormessage(mutuablelivedata: String)
    fun successmessage(mutuablelivedata: String)
    fun basicvehicleinfo(mutuablelivedata: vehicleinfoedit)
    fun arraylistofvehiclemodel(mutuablelivedata: ArrayList<VehicleListModel>)
    fun defaultvehiclemodel(mutuablelivedata: DefaultVehicleListModel)
    fun documentadded(mutuablelivedata: Int)

}