package com.cabilyhandyforalldinedoo.chd.ui.registeration

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import cabily.handyforall.dinedoo.R
import com.cabilyhandyforalldinedoo.chd.EventBusConnection.IntentServiceResult
import com.cabilyhandyforalldinedoo.chd.Modal.VehicleselectionModel
import com.cabilyhandyforalldinedoo.chd.SessionManagerPackage.SessionManager
import com.cabilyhandyforalldinedoo.chd.ViewModelRegisterModule.VehicleDetailsViewModel
import com.cabilyhandyforalldinedoo.chd.adaptersofchd.vechileselectionadapter
import com.cabilyhandyforalldinedoo.chd.clickableInterface.CustomOnClickListener
import com.cabilyhandyforalldinedoo.chd.commonutils.AppUtils
import com.cabilyhandyforalldinedoo.chd.commonutils.Constants
import kotlinx.android.synthetic.main.vehiclemodelselction.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.util.*

class VehicleDetailsSelection : LocaleAwareCompatActivity()
{
    //variable Decalaration
    private lateinit var countryadapater: vechileselectionadapter
    lateinit var mViewModel: VehicleDetailsViewModel
    private lateinit var mContext: Activity
    var fullcountryarray: ArrayList<VehicleselectionModel> ?= null

    private lateinit var condition: String
    private lateinit var locationid: String
    private lateinit var categoryid: String
    private lateinit var skipingid: String
    private lateinit var vehicletypeid: String
    private lateinit var makeid: String
    private lateinit var modelid: String

    var gotservicelocationresponse: String= ""


    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.vehiclemodelselction)
        mContext = this@VehicleDetailsSelection
        getExtraValue()
        initViewModel()
        editextlistenercall()

        // click listener
        close.setOnClickListener {
            AppUtils.hideKeyboard(mContext, clear!!)
            finish()
        }
        clear.setOnClickListener {
            searchtext.getText().clear()
            AppUtils.hideKeyboard(mContext, clear!!)
        }
    }


    private fun getExtraValue()
    {
        condition = intent.getStringExtra("vehicled")
        //locationid = intent.getStringExtra("locationid")
        locationid = intent.getStringExtra("locationid")
        skipingid = intent.getStringExtra("skipingid")
        categoryid = intent.getStringExtra("categoryid")
        vehicletypeid = intent.getStringExtra("vehicletypeid")
        makeid = intent.getStringExtra("makeid")
        modelid = intent.getStringExtra("modelid")
    }

    fun editextlistenercall()
    {
        searchtext.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if(fullcountryarray != null && fullcountryarray!!.size >0)
                {
                    mViewModel.filter(p0.toString(),fullcountryarray!!)
                }
            }
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (p0!!.length == 0)
                    clear.visibility = View.INVISIBLE
                else
                    clear.visibility = View.VISIBLE
            }
        })
    }
    private fun initrecyclerviews(countryarray: ArrayList<VehicleselectionModel>)
    {
        fullcountryarray=countryarray
        rv_todo_list.layoutManager = LinearLayoutManager(this)
        countryadapater = vechileselectionadapter(this,countryarray, object : CustomOnClickListener{
            override fun onItemClickListener(view: View, position: Int,id :String,name :String)
            {
                val intentz = Intent()
                intentz.putExtra(Constants.INTENT_OBJECT, id+","+name)
                setResult(Activity.RESULT_OK, intentz)
                finish()
            }
        })
        rv_todo_list.adapter = countryadapater
    }
    private fun initViewModel()
    {
        mViewModel = ViewModelProviders.of(this).get(VehicleDetailsViewModel::class.java)
        if(condition.equals("1"))
        {
            if(gotservicelocationresponse.equals(""))
            {
                loader.visibility = View.VISIBLE
                mViewModel.getservicelocation(mContext)
            }
            else
            {
                mViewModel.splilocationresponse(mContext,gotservicelocationresponse,skipingid)
            }
        }
        else if(condition.equals("2"))
        {
            loader.visibility = View.VISIBLE
            mViewModel.startcategoryfetchapi(mContext)
        }
        else if(condition.equals("3"))
        {
            loader.visibility = View.VISIBLE
            var mSessionManager: SessionManager? = null
            mSessionManager = SessionManager(mContext)
            if(mSessionManager!!.getCategoryDetails().equals(""))
            {
                mViewModel.startcategoryapi(mContext)
            }
            else
            {
                mViewModel.fullcategoryresponse(mContext,categoryid)
            }
        }
        else if(condition.equals("4"))
        {
            loader.visibility = View.VISIBLE
            mViewModel.fullcategorymakeresponse(mContext,vehicletypeid)
        }
        else if(condition.equals("5"))
        {
            loader.visibility = View.VISIBLE
            mViewModel.fullcategorymodelresponse(mContext,makeid,categoryid)
        }
        else if(condition.equals("6"))
        {
            loader.visibility = View.VISIBLE
            mViewModel.fullcategoryyearresponse(mContext,makeid,categoryid,modelid)
        }
        mViewModel.countryarrayobserver().observe(this, Observer {
            if(loader.getVisibility() == View.VISIBLE)
                loader.visibility = View.INVISIBLE
            initrecyclerviews(it)
        })
        mViewModel.filtercountryarrayobserver().observe(this, Observer {
            countryadapater.filterList(it)
        })
    }







    @Subscribe(threadMode = ThreadMode.MAIN)
    fun doThis(intentServiceResult: IntentServiceResult)
    {
        var finalresponse: String = intentServiceResult.resultValue
        loader.visibility = View.INVISIBLE
        var apiName: String = intentServiceResult.apiName
        if (apiName.equals(getString(R.string.getmakemodelyear)))
        {
            if (finalresponse != "failed")
            {
                if(condition.equals("2"))
                {
                    mViewModel.splitresponsecategory(mContext,finalresponse,locationid,skipingid)
                }
            }
            else
                commonerrorpage()
        }
        else if (apiName.equals(getString(R.string.getservicelocationresponse)))
        {
            if (finalresponse != "failed")
            {
                if(condition.equals("1"))
                {
                    gotservicelocationresponse = finalresponse
                    mViewModel.splilocationresponse(mContext,finalresponse,skipingid)
                }
            }
            else
                commonerrorpage()
        }
        else if (apiName.equals(getString(R.string.getonlymake)))
        {
            if (finalresponse != "failed")
            {
                mViewModel.fullcategoryresponse(mContext,categoryid)
            }
            else
                commonerrorpage()
        }
    }


    fun commonerrorpage()
    {
        AppUtils.commonerrorsheet(mContext,getString(R.string.failed),getString(R.string.failedproblem))
    }

    override fun onPause()
    {
        super.onPause()
        EventBus.getDefault().unregister(this);
    }
    override fun onResume()
    {
        super.onResume()
        EventBus.getDefault().register(this);
    }
    override fun onDestroy()
    {
        super.onDestroy()
        EventBus.getDefault().unregister(this);
    }


}
