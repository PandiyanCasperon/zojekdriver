package com.cabilyhandyforalldinedoo.chd.ui.sidemenus

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import cabily.handyforall.dinedoo.R
import cabily.handyforall.dinedoo.databinding.EarningpagethreelayoutBinding
import cabily.handyforall.dinedoo.databinding.EarningpagetwolayoutBinding
import com.cabilyhandyforalldinedoo.chd.EventBusConnection.IntentServiceResult
import com.cabilyhandyforalldinedoo.chd.adaptersofchd.Weeklyadapter
import com.cabilyhandyforalldinedoo.chd.clickableInterface.tripdetailclick
import com.cabilyhandyforalldinedoo.chd.commonutils.AppUtils
import com.cabilyhandyforalldinedoo.chd.earningsviewmodel.WeekModel
import com.cabilyhandyforalldinedoo.chd.earningsviewmodel.viewmodelforearningsthree
import com.cabilyhandyforalldinedoo.chd.earningsviewmodel.viewmodelforearningstwo
import com.cabilyhandyforalldinedoo.chd.ui.registeration.LocaleAwareCompatActivity
import kotlinx.android.synthetic.main.earningpagethreelayout.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.util.*


class earningpagethree : LocaleAwareCompatActivity ()
{

    private lateinit var mContext: Activity
    lateinit var binding: EarningpagethreelayoutBinding
    private lateinit var viewModel: viewmodelforearningsthree
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.earningpagethreelayout)
        mContext = this@earningpagethree
        initviewmodel()
        binding.setViewModel(viewModel)
        closeall.setOnClickListener {
            finish()
        }


    }
    //Viewmodel Observer Part
    fun initviewmodel()
    {
        viewModel = ViewModelProviders.of(this).get(viewmodelforearningsthree::class.java)
        binding.loader.visibility = View.VISIBLE
        viewModel.retrieveapicall(mContext)
        viewModel.gotdataforlist().observe(this, Observer {
            if(it != 1)
            {
                binding.loader.visibility = View.GONE
                AppUtils.commonerrorsheet(mContext,getString(R.string.failed),getString(R.string.failedproblem))
            }
        })



    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun doThis(intentServiceResult: IntentServiceResult)
    {
        var apiName: String = intentServiceResult.apiName
        if (apiName.equals(getString(R.string.retrievetripdetail)))
        {
            /*var response: String = intentServiceResult.resultValue
            if (response != "failed")  viewModel.splitapiresopnse(applicationContext,response)
            else
            {
                binding.loader.visibility = View.GONE
                AppUtils.commonerrorsheet(mContext,getString(R.string.failed),getString(R.string.failedproblem))
            }*/
        }
    }
    override fun onResume()
    {
        super.onResume()
        if(!EventBus.getDefault().isRegistered(this))  EventBus.getDefault().register(this)
    }
    override fun onDestroy()
    {
        super.onDestroy()
        if(EventBus.getDefault().isRegistered(this))   EventBus.getDefault().unregister(this)
    }

}