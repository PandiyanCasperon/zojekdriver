package com.cabilyhandyforalldinedoo.chd.clickableInterface

import android.view.View
import com.cabilyhandyforalldinedoo.chd.ViewModelWIthRepositaryMain.EarningModel


interface RecyclerViewClickListener {
    fun onRecyclerViewItemClick(view: View, earning: EarningModel)
}