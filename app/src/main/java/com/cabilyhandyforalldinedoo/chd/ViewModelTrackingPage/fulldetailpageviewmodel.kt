package com.cabilyhandyforalldinedoo.chd.ViewModelTrackingPage

import android.app.Application
import android.content.Context
import android.content.Intent
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import cabily.handyforall.dinedoo.R
import com.cabilyhandyforalldinedoo.chd.Backgroundservices.commonapifetchservice
import com.cabilyhandyforalldinedoo.chd.ViewModelTripDetail.FullTripDetailListener
import com.cabilyhandyforalldinedoo.chd.ViewModelTripDetail.TripDetailfullrepostiatry
import com.cabilyhandyforalldinedoo.chd.ViewModelTripDetail.fullridedetailsDataModel
import java.util.ArrayList


class fulldetailpageviewmodel(application: Application) : AndroidViewModel(application)
{
    var dashboarddata: TripDetailfullrepostiatry
    private val userpojoclass = MutableLiveData<fullridedetailsDataModel>()
    private val earninglistt = MutableLiveData<ArrayList<earningModel>>()
    private val passengerpaidd = MutableLiveData<ArrayList<passengerpaidModel>>()

    fun userpojoobserver(): MutableLiveData<fullridedetailsDataModel>
    {
        return userpojoclass
    }
    fun driverearningobserver(): MutableLiveData<ArrayList<earningModel>>
    {
        return earninglistt
    }
    fun passengerpaidobserver(): MutableLiveData<ArrayList<passengerpaidModel>>
    {
        return passengerpaidd
    }
    init
    {
        dashboarddata = TripDetailfullrepostiatry( object : FullTripDetailListener
        {
            override fun onPassengerPaid(mutableLiveData: ArrayList<passengerpaidModel>) {
                passengerpaidd.value = mutableLiveData
            }

            override fun onEarningList(mutableLiveData: ArrayList<earningModel>) {
                earninglistt.value = mutableLiveData
            }

            override fun onDataReceived(mutableLiveData: fullridedetailsDataModel) {
                userpojoclass.value = mutableLiveData            }

        })
    }

    //hitting api call
    fun getridedetails(mContext: Context, rideid: String)
    {
        val serviceClass = commonapifetchservice::class.java
        val intent = Intent(mContext, serviceClass)
        intent.putExtra(mContext.getString(R.string.intent_putextra_api_key), mContext.getString(R.string.retrievetripdetailfully))
        intent.putExtra("rideid", rideid)
        mContext.startService(intent)
    }

    //splitresponse
    fun splitapiresopnse(mContext: Context, response: String)
    {
        dashboarddata.splitrideinfo(response,mContext)
    }

}