package com.cabilyhandyforalldinedoo.chd.CustomTextViewPackage

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import android.widget.RadioButton
import android.widget.TextView


class RadioButtonTextViewNormal : RadioButton
{
    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle)
    {
        init()
    }
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    {
        init()
    }
    constructor(context: Context) : super(context)
    {
        init()
    }
    fun init()
    {
        val tf = Typeface.createFromAsset(context.assets, "fonts/proxima_nova_reg-webfont.ttf")
        typeface = tf
    }
}
