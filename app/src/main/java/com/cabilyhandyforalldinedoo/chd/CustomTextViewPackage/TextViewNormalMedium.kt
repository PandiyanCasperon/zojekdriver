package com.cabilyhandyforalldinedoo.chd.CustomTextViewPackage

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import android.widget.TextView


class TextViewNormalMedium : TextView
{
    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle)
    {
        init()
    }
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    {
        init()
    }
    constructor(context: Context) : super(context)
    {
        init()
    }
    fun init()
    {
        val tf = Typeface.createFromAsset(context.assets, "fonts/sanomat_zawgyi_regular.ttf")
        typeface = tf
    }
}
