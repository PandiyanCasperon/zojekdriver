package com.cabilyhandyforalldinedoo.chd.ViewModelRideRequest


import java.util.ArrayList

interface RideRequestListener {
    fun onDataReceived(mutableLiveData: ArrayList<ItemInfo>)
    fun onOnlinestatusChange(onlinesttau: String)
    fun onError(error: Int)
}