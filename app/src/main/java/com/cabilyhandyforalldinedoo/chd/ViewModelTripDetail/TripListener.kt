package com.cabilyhandyforalldinedoo.chd.ViewModelTripDetail


import java.util.ArrayList

interface TripListener {
    fun onDataReceived(mutableLiveData: ArrayList<TripDataModel>)
    fun filterDataReceived(mutableLiveData: ArrayList<TripDataModel>)
    fun onSuccessorFailed(mutableLiveData: Int)
}