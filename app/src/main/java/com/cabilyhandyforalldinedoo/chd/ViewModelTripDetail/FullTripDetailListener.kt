package com.cabilyhandyforalldinedoo.chd.ViewModelTripDetail

import com.cabilyhandyforalldinedoo.chd.ViewModelTrackingPage.earningModel
import com.cabilyhandyforalldinedoo.chd.ViewModelTrackingPage.passengerpaidModel
import java.util.ArrayList


interface FullTripDetailListener {
    fun onDataReceived(mutableLiveData: fullridedetailsDataModel)
    fun onEarningList(mutableLiveData: ArrayList<earningModel>)
    fun onPassengerPaid(mutableLiveData: ArrayList<passengerpaidModel>)
}