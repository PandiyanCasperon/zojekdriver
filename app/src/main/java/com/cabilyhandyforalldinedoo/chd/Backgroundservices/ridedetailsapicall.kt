package com.cabilyhandyforalldinedoo.chd.Backgroundservices


import android.app.Activity
import android.app.IntentService
import android.content.Intent
import android.os.IBinder
import cabily.handyforall.dinedoo.R
import com.cabilyhandyforalldinedoo.chd.EventBusConnection.IntentServiceResult
import com.cabilyhandyforalldinedoo.chd.SessionManagerPackage.SessionManager
import com.cabilyhandyforalldinedoo.chd.retrofit.RetrofitInstance
import com.mindorks.kotnetworking.KotNetworking
import com.mindorks.kotnetworking.common.Priority
import org.greenrobot.eventbus.EventBus
import org.json.JSONObject
import java.util.*


class ridedetailsapicall : IntentService("ApiHit")
{


    var responsetosend: String = "failed"
    var header = HashMap<String, String>()
    var urltohit: String = ""
    lateinit private var mSessionManager: SessionManager


    override fun onBind(intent: Intent): IBinder?
    {
        throw UnsupportedOperationException("Not yet implemented")
    }

    override fun onHandleIntent(intent: Intent?)
    {

        mSessionManager = SessionManager(applicationContext!!)

        header.put("driver_id", mSessionManager!!.getDriverId())
        header.put("ride_id", mSessionManager!!.getdutyrideid())

        // url to hit
        urltohit = RetrofitInstance.ridedetailss

        // common network call
        acknwoldegementcallhit(header)
    }

    // common api fetch service
    private fun acknwoldegementcallhit(header: HashMap<String, String>)
    {
        KotNetworking.post(urltohit)
                .addBodyParameter(header)
                .addHeaders(mSessionManager!!.getApiHeader())
                .setTag(this)
                .setPriority(Priority.HIGH)
                .build()
                .getAsString { response, error ->
                    if (error != null)
                    {
                        EventBus.getDefault().post(IntentServiceResult(Activity.RESULT_OK, responsetosend!!, getString(R.string.ridedetails)))
                    }
                    else
                    {
                        var response:String = response.toString()
                        var responseObj = JSONObject(response)
                        if (responseObj.has("response"))
                        {
                            var getingres: JSONObject? = responseObj.getJSONObject("response")
                            var mSessionManager: SessionManager? = null
                            mSessionManager = SessionManager(applicationContext!!)
                            val list = `getingres`!!.getJSONArray("list")
                            if (list.length() > 0)
                            {
                                for (i in 0 until list.length())
                                {
                                    var j_ride_object = list.getJSONObject(i)
                                    var ridestatuss:String  = j_ride_object!!.getString("btn_group")
                                    mSessionManager .onridedetails(response,ridestatuss,"1")


                                    EventBus.getDefault().post(IntentServiceResult(Activity.RESULT_OK, response, getString(R.string.ridedetails)))
                                }
                            }
                        }

                        else
                        {
                            EventBus.getDefault().post(IntentServiceResult(Activity.RESULT_OK, "0", getString(R.string.ridedetails)))
                        }
                    }
                }
    }






}