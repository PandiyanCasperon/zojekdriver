package com.cabilyhandyforalldinedoo.chd.Backgroundservices


import android.annotation.SuppressLint
import android.app.Activity
import android.app.IntentService
import android.content.Intent
import android.os.AsyncTask
import android.os.Build
import android.os.IBinder
import android.util.Base64
import android.util.Log
import cabily.handyforall.dinedoo.R
import com.cabilyhandyforalldinedoo.chd.EventBusConnection.IntentServiceReroutingRouteResult
import com.cabilyhandyforalldinedoo.chd.EventBusConnection.IntentServiceResult
import com.cabilyhandyforalldinedoo.chd.EventBusConnection.IntentServiceRouteResult
import com.cabilyhandyforalldinedoo.chd.SessionManagerPackage.SessionManager
import com.cabilyhandyforalldinedoo.chd.ViewModelRideRequest.RouteDataParser
import com.cabilyhandyforalldinedoo.chd.retrofit.RetrofitInstance
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.PolylineOptions
import com.google.firebase.iid.FirebaseInstanceId
import com.mindorks.kotnetworking.KotNetworking
import com.mindorks.kotnetworking.common.Priority
import okhttp3.ResponseBody
import org.greenrobot.eventbus.EventBus
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.nio.charset.Charset
import java.util.*


class commonapifetchservice : IntentService("ApiHit")
{
    lateinit private var mSessionManager: SessionManager
    private var apiCall: Call<ResponseBody>? = null
    var responsetosend: String = "failed"
    var api_name: String = ""
    lateinit var waypoints_array: ArrayList<LatLng>
    private var stops_location_array: ArrayList<String>? = null

    var header = HashMap<String, String>()
    var urltohit: String = ""

    // mobily verify call
    var mobmobileno: String = ""
    var mobcode: String = ""

    // mobily verify call
    var otpmobileno: String = ""
    var otpcode: String = ""

    override fun onBind(intent: Intent): IBinder?
    {
        throw UnsupportedOperationException("Not yet implemented")
    }
    override fun onHandleIntent(intent: Intent?)
    {
        waypoints_array = ArrayList()
        stops_location_array = ArrayList()
        mSessionManager = SessionManager(applicationContext!!)
        if (intent?.hasExtra(getString(R.string.intent_putextra_api_key))!!)
        {
            api_name = intent?.getStringExtra(getString(R.string.intent_putextra_api_key))

            if (api_name.equals(getString(R.string.getappinfohit)))
            {
                getfcmtoken()

                // header value of app more info
                header["user_type"] = "driver"
                header["driver_id"] = mSessionManager!!.getDriverId()

                // url to hit
                urltohit = RetrofitInstance.appmoreinfo
                // common network call
                commonapihit(header,api_name)
            }
            else if (api_name.equals(getString(R.string.registerpageoneapi)))
            {
                // mobile login check api
                getfcmtoken()

                mobmobileno = intent?.getStringExtra("mobileno")
                mobcode = intent?.getStringExtra("code")

                // header value of phoneverify
                header.put("mobile_number", mobmobileno)
                header.put("dial_code", mobcode)

                // url to hit
                urltohit = RetrofitInstance.verifymobile

                // common network call
                commonapihit(header,api_name)
            }
            else if (api_name.equals(getString(R.string.tripservice)))
            {

                header.put("driver_id", mSessionManager!!.getDriverId())
                header.put("booking_timestamp", "")
                header.put("record", "")

                // url to hit
                urltohit = RetrofitInstance.triplist

                // common network call
                commonapihit(header,api_name)

            }
            else if (api_name.equals(getString(R.string.startfirstorlast)))
            {
                val bookingdateandtime = intent.getStringExtra("bookingdateandtime")
                header.put("driver_id", mSessionManager!!.getDriverId())
                header.put("booking_timestamp", bookingdateandtime)
                header.put("record", "first")

                // url to hit
                urltohit = RetrofitInstance.triplist

                // common network call
                commonapihit(header,api_name)
            }
            else if (api_name.equals(getString(R.string.startlast)))
            {
                val bookingdateandtime = intent.getStringExtra("bookingdateandtime")
                header.put("driver_id", mSessionManager!!.getDriverId())
                header.put("booking_timestamp", bookingdateandtime)
                header.put("record", "last")

                // url to hit
                urltohit = RetrofitInstance.triplist

                // common network call
                commonapihit(header,api_name)
            }
            else if (api_name.equals(getString(R.string.updatemobilenumber)))
            {

                mobmobileno = intent?.getStringExtra("mobileno")
                mobcode = intent?.getStringExtra("code")

                // header value of phoneverify
                header.put("phone_number", mobmobileno)
                header.put("country_code", mobcode)
                header.put("driver_id", mSessionManager!!.getDriverId())
                header.put("otp", "")

                // url to hit
                urltohit = RetrofitInstance.updatemobileno

                // common network call
                commonapihit(header,api_name)
            }
            else if (api_name.equals(getString(R.string.updateemailidalone)))
            {

                var email = intent?.getStringExtra("email")


                // header value of phoneverify
                header.put("email", email)
                header.put("driver_id", mSessionManager!!.getDriverId())
                header.put("otp", "")

                // url to hit
                urltohit = RetrofitInstance.updateemailid

                // common network call
                commonapihit(header,api_name)
            }
            else if (api_name.equals(getString(R.string.updateemailidalonewithotp)))
            {

                var email = intent?.getStringExtra("email")
                var otp = intent?.getStringExtra("otp")



                // header value of phoneverify
                header.put("email", email)
                header.put("driver_id", mSessionManager!!.getDriverId())
                header.put("otp",otp)

                // url to hit
                urltohit = RetrofitInstance.updateemailid

                // common network call
                commonapihit(header,api_name)
            }
            else if (api_name.equals(getString(R.string.updatemobilewitotp)))
            {

                mobmobileno = intent?.getStringExtra("mobile_number")
                mobcode = intent?.getStringExtra("dail_code")
                var otp = intent?.getStringExtra("otp")

                // header value of phoneverify
                header.put("phone_number", mobmobileno)
                header.put("country_code", mobcode)
                header.put("driver_id", mSessionManager!!.getDriverId())
                header.put("otp", otp)

                // url to hit
                urltohit = RetrofitInstance.updatemobileno

                // common network call
                commonapihit(header,api_name)
            }
            else if (api_name.equals(getString(R.string.cashreceived)))
            {
                var ride_id = intent?.getStringExtra("ride_id")
                // header value of phoneverify
                header.put("ride_id", ride_id)
                header.put("driver_id", mSessionManager!!.getDriverId())
                header.put("amount", intent?.getStringExtra("amount"))

                // url to hit
                urltohit = RetrofitInstance.receivecash

                // common network call
                commonapihit(header,api_name)
            }
            else if (api_name.equals(getString(R.string.defaultvehicleupdte)))
            {
                var vehicle_id = intent?.getStringExtra("vehicleid")
                // header value of phoneverify
                header.put("vehicle_id", vehicle_id)
                header.put("driver_id", mSessionManager!!.getDriverId())

                // url to hit
                urltohit = RetrofitInstance.defaultvechicleupdate

                // common network call
                commonapihit(header,api_name)
            }
            else if (api_name.equals(getString(R.string.getvehicleinfoforedit)))
            {
                var vehicle_id = intent?.getStringExtra("vehicleid")
                // header value of phoneverify
                header.put("vehicle_id", vehicle_id)
                header.put("driver_id", mSessionManager!!.getDriverId())

                // url to hit
                urltohit = RetrofitInstance.editvehicleinfo

                // common network call
                commonapihit(header,api_name)
            }
            else if (api_name.equals(getString(R.string.getdriverdocument)))
            {
                header.put("driver_id", mSessionManager!!.getDriverId())

                // url to hit
                urltohit = RetrofitInstance.driverdocinfo

                // common network call
                commonapihit(header,api_name)
            }
            else if (api_name.equals(getString(R.string.getvehiclelist)))
            {

                header.put("driver_id", mSessionManager!!.getDriverId())

                // url to hit
                urltohit = RetrofitInstance.getvehiclelistt

                // common network call
                commonapihit(header,api_name)
            }
            else if (api_name.equals(getString(R.string.retrievetripdetail)))
            {
                var ride_id = intent?.getStringExtra("ride_id")
                // header value of tripdetails
                header.put("driver_id", mSessionManager!!.getDriverId())
                header.put("trip_type", "all")

                // url to hit
                urltohit = RetrofitInstance.tripdetailss

                // common network call
                commonapihit(header,api_name)
            }
            else if (api_name.equals(getString(R.string.retrievetripdetailfully)))
            {
                var ride_id = intent?.getStringExtra("rideid")
                // header value of tripdetails full
                header.put("driver_id", mSessionManager!!.getDriverId())
                header.put("ride_id", ride_id)

                // url to hit
                urltohit = RetrofitInstance.tripdetailssfull

                // common network call
                commonapihit(header,api_name)
            }

            else if (api_name.equals(getString(R.string.resendotp)))
            {
                // otp resend api
                otpmobileno = intent?.getStringExtra("mobileno")
                otpcode = intent?.getStringExtra("code")

                // header value of otp
                header.put("mobile_number", otpmobileno)
                header.put("dial_code", otpcode)
                header.put("type", "resend")

                // url to hit
                urltohit = RetrofitInstance.resendotp

                // common network call
                commonapihit(header,api_name)
            }
            else if (api_name.equals(getString(R.string.getservicelocationresponse)) || api_name.equals(getString(R.string.getmakemodelyear)))
            {
                // url to hit
                urltohit = RetrofitInstance.servicelocationapi

                // common network call
                commonapihit(header,api_name)
            }
            else if (api_name.equals(getString(R.string.cancelreason)))
            {

                // header value of phoneverify
                header.put("driver_id", mSessionManager!!.getDriverId())
                header.put("ride_id", intent?.getStringExtra("rideid"))
                header.put("user_type", "driver")

                // url to hit
                urltohit = RetrofitInstance.cancelreason

                // common network call
                commonapihit(header,api_name)
            }
            else if (api_name.equals(getString(R.string.chatmessageurl)))
            {


                val job = JSONObject()
                job.put("action", "dectar_chat")
                job.put("type", "OTHER")
                job.put("sender_ID", intent?.getStringExtra("useridtosend"))
                job.put("ride_id", intent?.getStringExtra("rideid"))
                job.put("desc", intent?.getStringExtra("message"))
                job.put("driver_image", "")
                job.put("driver_name", "")
                job.put("voice_timing", intent?.getStringExtra("timestamp"))
                job.put("time_stamp", intent?.getStringExtra("timestamp"))


                // header value of chatmessage
                header.put("id", mSessionManager!!.getDriverId())
                header.put("user_type", "driver")
                header.put("ride_id", intent?.getStringExtra("rideid"))
                header.put("message_type", "text")
                header.put("message_content", job.toString())
                header.put("message", job.toString())
                header.put("message", intent?.getStringExtra("message"))
                header.put("timestamp", intent?.getStringExtra("timestamp"))
                header.put("time_stamp", intent?.getStringExtra("timestamp"))


                // url to hit
                urltohit = RetrofitInstance.chaturl

                // common network call
                commonapihit(header,api_name)
            }

            else if (api_name.equals(getString(R.string.cancelthisride)))
            {

                // header value of phoneverify
                header.put("driver_id", mSessionManager!!.getDriverId())
                header.put("ride_id", intent?.getStringExtra("rideid"))
                header.put("reason_id",intent?.getStringExtra("cancelid"))
                header.put("user_type", "driver")

                // url to hit
                urltohit = RetrofitInstance.cancelthisride

                // common network call
                commonapihit(header,api_name)
            }

            else if (api_name.equals(getString(R.string.finalcalltohomepage)))
            {

                // header value of phoneverify
                header.put("driver_id", mSessionManager!!.getDriverId())
                header.put("vehicle_id", mSessionManager!!.gettempvehicleid())
                header.put("type",intent?.getStringExtra("type"))


                // url to hit
                urltohit = RetrofitInstance.fianalmovepage

                // common network call
                commonapihit(header,api_name)
            }

            else if (api_name.equals(getString(R.string.emailcheckk)))
            {

                val emailid = intent?.getStringExtra("emailid")

                // header value of verifyemail
                header.put("email", emailid)

                // url to hit
                urltohit = RetrofitInstance.verifyemail

                // common network call
                commonapihit(header,api_name)
            }
            else if (api_name.equals(getString(R.string.checkinuserexist)))
            {

                getfcmtoken()

                // header value of checking user
                if(intent?.getStringExtra("dail_code").startsWith("+"))
                    header.put("dial_code", intent?.getStringExtra("dail_code"))
                else
                    header.put("dial_code", "+"+intent?.getStringExtra("dail_code"))

                header.put("mobile_number", intent?.getStringExtra("mobile_number"))
                header.put("gcm_id", mSessionManager!!.getfcmkey())
                header.put("latitude", "0.0")
                header.put("longitude", "0.0")

                // url to hit
                urltohit = RetrofitInstance.checkuserexist

                // common network call
                commonapihit(header,api_name)
            }
            else if (api_name.equals(getString(R.string.locationfetchapi)))
            {
                // url to hit

                // common network call
                commonapihit(header,api_name)
            }
            else if (api_name.equals(getString(R.string.steptwoapi)))
            {
                // url to hit document
                urltohit = RetrofitInstance.getdocument

                // common network call
                commonapihit(header,api_name)
            }


            else if (api_name.equals(getString(R.string.verifyvehiclenumber)))
            {
                // header value of finaldocumentsubmit
                val params = intent.getSerializableExtra("params") as HashMap<String, String>

                header=params


                // url to hit final reg call
                urltohit = RetrofitInstance.getvehicleid

                // common network call
                commonapihit(header,api_name)
            }
            else if (api_name.equals(getString(R.string.basicdriverprofileupdate)))
            {
                // header value of finaldocumentsubmit
                val params = intent.getSerializableExtra("params") as HashMap<String, String>
                header=params

                // url to hit final reg call
                urltohit = RetrofitInstance.editprofile

                // common network call
                commonapihit(header,api_name)
            }
            else if (api_name.equals(getString(R.string.finaldocument)))
            {
                // header value of finaldocumentsubmit
                val params = intent.getSerializableExtra("params") as HashMap<String, String>
                header=params
                header.put("driver_id",mSessionManager!!.getDriverId())
                header.put("vehicle_id",mSessionManager!!.gettempvehicleid())

                // url to hit final reg call
                urltohit = RetrofitInstance.basicregister

                // common network call
                commonapihit(header,api_name)
            }

            else if (api_name.equals(getString(R.string.finaldocumentupload)))
            {
                // header value of finaldocumentsubmit
                val params = intent.getSerializableExtra("params") as HashMap<String, String>
                header=params
                header.put("driver_id",mSessionManager!!.getDriverId())
                var categroy=intent?.getStringExtra("categroy")
                if(categroy.equals("Vehicle") || categroy.equals("vehicle"))
                {
                    header.put("vehicle_id",mSessionManager!!.gettempvehicleid())
                }
                else
                {
                    header.put("vehicle_id","")
                }



                // url to hit final reg call
                urltohit = RetrofitInstance.documentsubmit

                // common network call
                commonapihit(header,api_name)
            }

            else if (api_name.equals(getString(R.string.driveravailable)))
            {
                var availablestatus = intent?.getStringExtra("availablestatus")

                header.put("driver_id",mSessionManager!!.getDriverId())
                header.put("work_status", availablestatus)

                // url to hit final reg call
                urltohit = RetrofitInstance.onlineofflinecall

                // common network call
                commonapihit(header,api_name)
            }

            else if (api_name.equals(getString(R.string.dashboarapicall)))
            {

                header.put("driver_id",mSessionManager!!.getDriverId())
                header.put("driver_lat","0.0")
                header.put("driver_lon","0.0")

                // url to hit
                urltohit = RetrofitInstance.dashboardcallapi

                // common network call
                commonapihit(header,api_name)
            }

            else if (api_name.equals(getString(R.string.routeapicall)))
            {

                var ctlat:Double  =   mSessionManager.getOnlineLatitiude().toDouble()
                var ctlong:Double =  mSessionManager.getOnlineLongitude().toDouble()

                var droplat:Double = intent?.getStringExtra("droplat").toDouble()
                var droplong:Double = intent?.getStringExtra("droplong").toDouble()

                val mycuurentloc = LatLng(ctlat, ctlong)
                val deslat = LatLng(droplat, droplong)
                val routeUrl = getRouteApiUrl(mycuurentloc, deslat)
                fetchRouteUrl(routeUrl)
            }

            else if (api_name.equals(getString(R.string.reroutingapicall)))
            {

                var ctlat:Double  =   mSessionManager.getOnlineLatitiude().toDouble()
                var ctlong:Double =  mSessionManager.getOnlineLongitude().toDouble()

                var droplat:Double = intent?.getStringExtra("droplat").toDouble()
                var droplong:Double = intent?.getStringExtra("droplong").toDouble()

                val mycuurentloc = LatLng(ctlat, ctlong)
                val deslat = LatLng(droplat, droplong)
                val routeUrl = getRouteApiUrl(mycuurentloc, deslat)
                fetchreroutingRouteUrl(routeUrl)
            }

          /*  else if (api_name.equals(getString(R.string.getmakemodelyear)))
            {
                // url to hit
                urltohit = RetrofitInstance.dummycategory

               *//* // header value of dummycat
                header.put("Authkey", "1534599312")*//*

                // common network call
                dummycommonapihit(header)
            }*/


            else if (api_name.equals(getString(R.string.getonlymake)))
            {
                // url to hit
                urltohit = RetrofitInstance.getcatmakemodel
                // common network call
                dummycommonapihit(header)
            }
        }








        else if (api_name.equals(getString(R.string.profilesaveapi)))
        {
            // otp resend api
            var dial_code = intent?.getStringExtra("dial_code")
            var mobile_number = intent?.getStringExtra("mobile_number")
            var email = intent?.getStringExtra("email")
            var first_name = intent?.getStringExtra("first_name")
            var last_name = intent?.getStringExtra("last_name")
            var image = intent?.getStringExtra("image")
            var gender = intent?.getStringExtra("gender")
            var dob = intent?.getStringExtra("dob")
            profilesend(dial_code,mobile_number,email,first_name,last_name,image,gender,dob)
        }
        else if (api_name.equals(getString(R.string.categoryfetchapi)))
        {
            getfcmtoken()
            categoryfetchapi()
        }
        else if (api_name.equals(getString(R.string.steponeapi)))
        {
            steponefetchapi()
        }

        else if (api_name.equals(getString(R.string.imageupload)))
        {
            var base64string = intent?.getStringExtra("base64string")
            imageuploadapi(base64string)
        }

        else if (api_name.equals(getString(R.string.starverficationapi)))
        {
            startverficationapi()
        }

        else if (api_name.equals(getString(R.string.reuploadapi)))
        {
            var id = intent?.getStringExtra("id")
            var image = intent?.getStringExtra("image")
            var name = intent?.getStringExtra("name")
            var type = intent?.getStringExtra("type")
            var choosedate = intent?.getStringExtra("choosedate")
            reuploadimageapi(id,image,name,type,choosedate)
        }

        else if (api_name.equals(getString(R.string.acknowlcall)))
        {
            var ack = intent?.getStringExtra("ack")
            var ride_id = intent?.getStringExtra("ride_id")
            acknwoldegementcallhit(ride_id,ack)
        }
    }





    // mobile login check
    private fun mobilenologincheck(mobileno:String,code:String)
    {
        KotNetworking.post(RetrofitInstance.verifymobile)
                .addBodyParameter("mobile_number",mobileno)
                .addBodyParameter("dail_code", code)
                .addHeaders(mSessionManager!!.getApiHeader())
                .setTag(this)
                .setPriority(Priority.MEDIUM)
                .build()
                .setAnalyticsListener { timeTakenInMillis, bytesSent, bytesReceived, isFromCache ->
                }
                .getAsString { response, error ->
                    if (error != null)
                    {
                        EventBus.getDefault().post(IntentServiceResult(Activity.RESULT_OK, responsetosend!!, getString(R.string.registerpageoneapi)))
                    }
                    else
                    {
                        responsetosend = response.toString()
                        EventBus.getDefault().post(IntentServiceResult(Activity.RESULT_OK, responsetosend!!, getString(R.string.registerpageoneapi)))

                    }
                }
    }

    // otp resend api
    private fun otpresned(mobileno:String,code:String)
    {
        var params = HashMap<String, String>()
        params["mobile_number"] = mobileno
        params["dail_code"] = "+"+code
        params["type"] = "register"
        apiCall = RetrofitInstance.getInstance().resendotp(mSessionManager!!.getApiHeader(), params)
        apiCall?.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>)
            {
                try
                {
                    if (response.isSuccessful)
                    {
                        if (response.body() != null)
                        {
                            var responseStr = response.body()?.string()
                            responsetosend = responseStr.toString()
                            EventBus.getDefault().post(IntentServiceResult(Activity.RESULT_OK, responsetosend!!, getString(R.string.resendotp)))
                        }
                    }
                }
                catch (e: Exception)
                {
                    e.printStackTrace()
                    EventBus.getDefault().post(IntentServiceResult(Activity.RESULT_OK, responsetosend!!, getString(R.string.resendotp)))
                }
            }
            override fun onFailure(call: Call<ResponseBody>, t: Throwable)
            {
                EventBus.getDefault().post(IntentServiceResult(Activity.RESULT_OK, responsetosend!!, getString(R.string.resendotp)))
            }
        })
    }

    // otp resend api
    private fun profilesend(code: String,mobileno: String,email:String,first_name:String,last_name:String,image:String,gender:String,dob:String)
    {
        var params = HashMap<String, String>()
        params["mobile_number"] = mobileno
        params["dial_code"]     = "+"+code
        params["email"]         = email
        params["first_name"]    = first_name
        params["last_name"]     = last_name
        params["image"]         = "data:image/png;base64,"+image
        params["gender"]        = gender
        params["dob"]           = dob
        params["token"]         = mSessionManager.getfcmkey()



        apiCall = RetrofitInstance.getInstance().sendprofiledetails(mSessionManager!!.getApiHeader(), params)
        apiCall?.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>)
            {
                try
                {
                    if (response.isSuccessful)
                    {
                        if (response.body() != null)
                        {
                            var responseStr = response.body()?.string()
                            responsetosend = responseStr.toString()
                            EventBus.getDefault().post(IntentServiceResult(Activity.RESULT_OK, responsetosend!!, getString(R.string.profilesaveapi)))
                        }
                    }
                }
                catch (e: Exception)
                {
                    e.printStackTrace()
                    EventBus.getDefault().post(IntentServiceResult(Activity.RESULT_OK, responsetosend!!, getString(R.string.profilesaveapi)))
                }
            }
            override fun onFailure(call: Call<ResponseBody>, t: Throwable)
            {
                EventBus.getDefault().post(IntentServiceResult(Activity.RESULT_OK, responsetosend!!, getString(R.string.profilesaveapi)))
            }
        })
    }


    // location api
    private fun locationfetchapi()
    {
        var params = HashMap<String, String>()
        apiCall = RetrofitInstance.getInstance().locationfetchapi(mSessionManager!!.getApiHeader(), params)
        apiCall?.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>)
            {
                try
                {
                    if (response.isSuccessful)
                    {
                        if (response.body() != null)
                        {
                            var responseStr = response.body()?.string()
                            responsetosend = responseStr.toString()
                            EventBus.getDefault().post(IntentServiceResult(Activity.RESULT_OK, responsetosend!!, getString(R.string.locationfetchapi)))
                        }
                    }
                }
                catch (e: Exception)
                {
                    e.printStackTrace()
                    EventBus.getDefault().post(IntentServiceResult(Activity.RESULT_OK, responsetosend!!, getString(R.string.locationfetchapi)))
                }
            }
            override fun onFailure(call: Call<ResponseBody>, t: Throwable)
            {
                EventBus.getDefault().post(IntentServiceResult(Activity.RESULT_OK, responsetosend!!, getString(R.string.locationfetchapi)))
            }
        })
    }


    // categoryfetchapi
    private fun categoryfetchapi()
    {
        var params = HashMap<String, String>()
        apiCall = RetrofitInstance.getInstance().categoryfetchapi(mSessionManager!!.getApiHeader(), params)
        apiCall?.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>)
            {
                try
                {
                    if (response.isSuccessful)
                    {
                        if (response.body() != null)
                        {
                            var responseStr = response.body()?.string()
                            responsetosend = responseStr.toString()
                            val response_json_object = JSONObject(responsetosend)
                            try
                            {
                                val status = response_json_object.getString("status")
                                if (status.equals("1"))
                                {
                                    mSessionManager = SessionManager(applicationContext!!)
                                    mSessionManager!!.setCategoryDetails(responsetosend)
                                }
                            }
                            catch (e: Exception)
                            {
                            }
                        }
                    }
                }
                catch (e: Exception)
                {
                    e.printStackTrace()

                }
            }
            override fun onFailure(call: Call<ResponseBody>, t: Throwable)
            {

            }
        })
    }


    // getappinfo api
    private fun getappinfodetails()
    {
        var params = HashMap<String, String>()

        params["user_type"] = "driver"
        params["driver_id"] = mSessionManager!!.getDriverId()
        apiCall = RetrofitInstance.getInstance().getappinfodetails(mSessionManager!!.getApiHeader(), params)
        apiCall?.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>)
            {
                try
                {

                }
                catch (e: Exception)
                {
                    e.printStackTrace()
                }
            }
            override fun onFailure(call: Call<ResponseBody>, t: Throwable)
            {
            }
        })
    }

    // driverdashboard api
    private fun driverdashboardapi()
    {
        var params = HashMap<String, String>()
        params["driver_id"] = mSessionManager!!.getDriverId()
        apiCall = RetrofitInstance.getInstance().driverdashboardapi(mSessionManager!!.getApiHeader(), params)
        apiCall?.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>)
            {
                try
                {
                    if (response.isSuccessful)
                    {
                        if (response.body() != null)
                        {
                            var responseStr = response.body()?.string()
                            responsetosend = responseStr.toString()
                            EventBus.getDefault().post(IntentServiceResult(Activity.RESULT_OK, responsetosend!!, getString(R.string.dashboarapicall)))
                        }
                    }
                }
                catch (e: Exception)
                {
                    e.printStackTrace()
                    EventBus.getDefault().post(IntentServiceResult(Activity.RESULT_OK, responsetosend!!, getString(R.string.dashboarapicall)))
                }
            }
            override fun onFailure(call: Call<ResponseBody>, t: Throwable)
            {
                EventBus.getDefault().post(IntentServiceResult(Activity.RESULT_OK, responsetosend!!, getString(R.string.dashboarapicall)))
            }
        })
    }





    // acknwoldegementcallhit  api
    private fun acknwoldegementcallhit(rideid:String,ack:String)
    {
        var params = HashMap<String, String>()
        params["driver_id"] = mSessionManager!!.getDriverId()
        params["ride_id"] = rideid
        params["action"] = ack
        apiCall = RetrofitInstance.getInstance().ackapi(mSessionManager!!.getApiHeader(), params)
        apiCall?.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>)
            {
                try
                {
                    if (response.isSuccessful)
                    {
                        if (response.body() != null)
                        {
                            var responseStr = response.body()?.string()
                            responsetosend = responseStr.toString()
                        }
                    }
                }
                catch (e: Exception)
                {
                    e.printStackTrace()
                }
            }
            override fun onFailure(call: Call<ResponseBody>, t: Throwable)
            {
            }
        })
    }

    // location api
    private fun steponefetchapi()
    {
        var params = HashMap<String, String>()
        params["id"] = mSessionManager!!.getDriverId()
        params["category"] = mSessionManager!!.getCategoryID()
        params["location"] = mSessionManager!!.getLocationID()
        params["vehicle_number"] = mSessionManager!!.getVehicleno()
        params["vehicle_type"] = mSessionManager!!.getVehicleTypeID()
        params["vehicle_maker"] = mSessionManager!!.getMakeID()
        params["vehicle_model"] = mSessionManager!!.getModelID()
        params["vehicle_model_year"] = mSessionManager!!.getYearID()
        params["vehicle_id"] = mSessionManager!!.getVehicleStepOneId()

        apiCall = RetrofitInstance.getInstance().savestep1(mSessionManager!!.getApiHeader(), params)
        apiCall?.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>)
            {
                try
                {
                    if (response.isSuccessful)
                    {
                        if (response.body() != null)
                        {
                            var responseStr = response.body()?.string()
                            responsetosend = responseStr.toString()
                            EventBus.getDefault().post(IntentServiceResult(Activity.RESULT_OK, responsetosend!!, getString(R.string.steponeapi)))
                        }
                    }
                }
                catch (e: Exception)
                {
                    e.printStackTrace()
                    EventBus.getDefault().post(IntentServiceResult(Activity.RESULT_OK, responsetosend!!, getString(R.string.steponeapi)))
                }
            }
            override fun onFailure(call: Call<ResponseBody>, t: Throwable)
            {
                EventBus.getDefault().post(IntentServiceResult(Activity.RESULT_OK, responsetosend!!, getString(R.string.steponeapi)))
            }
        })
    }


    // steptwodocument api
    private fun steptwodocument()
    {
        var params = HashMap<String, String>()
        apiCall = RetrofitInstance.getInstance().getdocument(mSessionManager!!.getApiHeader(), params)
        apiCall?.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>)
            {
                try
                {
                    if (response.isSuccessful)
                    {
                        if (response.body() != null)
                        {
                            var responseStr = response.body()?.string()
                            responsetosend = responseStr.toString()
                            EventBus.getDefault().post(IntentServiceResult(Activity.RESULT_OK, responsetosend!!, getString(R.string.steptwoapi)))
                        }
                    }
                }
                catch (e: Exception)
                {
                    e.printStackTrace()
                    EventBus.getDefault().post(IntentServiceResult(Activity.RESULT_OK, responsetosend!!, getString(R.string.steptwoapi)))
                }
            }
            override fun onFailure(call: Call<ResponseBody>, t: Throwable)
            {
                EventBus.getDefault().post(IntentServiceResult(Activity.RESULT_OK, responsetosend!!, getString(R.string.steptwoapi)))
            }
        })
    }


    // imageupload api
    private fun imageuploadapi(base64string:String)
    {
        var params = HashMap<String, String>()
        params["image"] = base64string
        apiCall = RetrofitInstance.getInstance().imageuploadpai(mSessionManager!!.getApiHeader(), params)
        apiCall?.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>)
            {
                try
                {
                    if (response.isSuccessful)
                    {
                        if (response.body() != null)
                        {
                            var responseStr = response.body()?.string()
                            responsetosend = responseStr.toString()
                            EventBus.getDefault().post(IntentServiceResult(Activity.RESULT_OK, responsetosend!!, getString(R.string.imageupload)))
                        }
                    }
                }
                catch (e: Exception)
                {
                    e.printStackTrace()
                    EventBus.getDefault().post(IntentServiceResult(Activity.RESULT_OK, responsetosend!!, getString(R.string.imageupload)))
                }
            }
            override fun onFailure(call: Call<ResponseBody>, t: Throwable)
            {
                EventBus.getDefault().post(IntentServiceResult(Activity.RESULT_OK, responsetosend!!, getString(R.string.imageupload)))
            }
        })
    }









    // startverficationapi api
    private fun startverficationapi()
    {
        var params = HashMap<String, String>()
        params["driver_id"] = mSessionManager!!.getDriverId()
        apiCall = RetrofitInstance.getInstance().verficationcall(mSessionManager!!.getApiHeader(), params)
        apiCall?.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>)
            {
                try
                {
                    if (response.isSuccessful)
                    {
                        if (response.body() != null)
                        {
                            var responseStr = response.body()?.string()
                            responsetosend = responseStr.toString()
                            EventBus.getDefault().post(IntentServiceResult(Activity.RESULT_OK, responsetosend!!, getString(R.string.starverficationapi)))
                        }
                    }
                }
                catch (e: Exception)
                {
                    e.printStackTrace()
                    EventBus.getDefault().post(IntentServiceResult(Activity.RESULT_OK, responsetosend!!, getString(R.string.starverficationapi)))
                }
            }
            override fun onFailure(call: Call<ResponseBody>, t: Throwable)
            {
                EventBus.getDefault().post(IntentServiceResult(Activity.RESULT_OK, responsetosend!!, getString(R.string.starverficationapi)))
            }
        })
    }


    // reuploadimageapi api
    private fun reuploadimageapi( id: String, image: String, name:String, type:String, choosedate:String)
    {
        var params = HashMap<String, String>()
        params["driver_id"] = mSessionManager!!.getDriverId()
        params["document_id"] = id
        params["image"] = "data:image/png;base64,"+image
        params["doc_name"] = name
        params["doc_type"] = type
        params["vehicle_id"] = mSessionManager!!.getVehicleStepOneId()
        params["expiry_date"] =choosedate
        params["document"] =""

        apiCall = RetrofitInstance.getInstance().reuploadimage(mSessionManager!!.getApiHeader(), params)
        apiCall?.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>)
            {
                try
                {
                    if (response.isSuccessful)
                    {
                        if (response.body() != null)
                        {
                            var responseStr = response.body()?.string()
                            responsetosend = responseStr.toString()
                            EventBus.getDefault().post(IntentServiceResult(Activity.RESULT_OK, responsetosend!!, getString(R.string.reuploadapi)))
                        }
                    }
                }
                catch (e: Exception)
                {
                    e.printStackTrace()
                    EventBus.getDefault().post(IntentServiceResult(Activity.RESULT_OK, responsetosend!!, getString(R.string.reuploadapi)))
                }
            }
            override fun onFailure(call: Call<ResponseBody>, t: Throwable)
            {
                EventBus.getDefault().post(IntentServiceResult(Activity.RESULT_OK, responsetosend!!, getString(R.string.reuploadapi)))
            }
        })
    }

    private fun getRouteApiUrl(origin: LatLng, dest: LatLng): String {

        // Origin of route
        val str_origin = "origin=" + origin.latitude + "," + origin.longitude
        // Destination of route
        val str_dest = "destination=" + dest.latitude + "," + dest.longitude
        // Sensor enabled
        val sensor = "sensor=false&mode=driving"
        //waypoints

        var waypoints: String = ""
        for (i in 0 until waypoints_array.size) {
            if (i == 0)
                waypoints = "waypoints="
            waypoints += "via:" + waypoints_array.get(i).latitude.toString() + "," + waypoints_array.get(i).longitude.toString() + "|"
        }
        // api_key
        val api_key = "key=" + resources.getString(R.string.routeKey)
        // Building the parameters to the web service
        val parameters = "$str_origin&$str_dest&$sensor&$waypoints&$api_key"
        // Output format
        val output = "json"
        // Building the url to the web service
        val url = "https://maps.googleapis.com/maps/api/directions/$output?$parameters"
        return url
    }
    private fun fetchRouteUrl(url: String) {

        val responseCall = RetrofitInstance.getGoogleInstance().getRouteList(url)
        responseCall.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    if (response.body() != null) {
                        // mLoader?.dismiss()
                        try {
                            val responseStr = response.body()!!.string()
                            Log.e("getAddressListRes", responseStr)
                            val parserTask = ParserTask()
                            // Invokes the thread for parsing the JSON data
                            if (Build.VERSION.SDK_INT >= 11) {
                                parserTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,responseStr)
                            } else {
                                parserTask.execute(responseStr)
                            }

                        } catch (e: Exception) {
                            e.printStackTrace()
                        }

                    }
                }
                else
                {
                    Log.e("getAddressListRes", "Failed")
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                EventBus.getDefault().post(IntentServiceResult(Activity.RESULT_OK, "0", getString(R.string.mapfailedtointernet)))
            }
        })
    }
    @SuppressLint("StaticFieldLeak")
    private inner class ParserTask : AsyncTask<String, Int, List<List<HashMap<String, String>>>>() {

        // Parsing the data in non-ui thread
        override fun doInBackground(vararg jsonData: String): List<List<HashMap<String, String>>> {
            val jObject: JSONObject
            try {
                jObject = JSONObject(jsonData[0])
                Log.d("ParserTask", jsonData[0])
                val parser = RouteDataParser()
                Log.d("ParserTask", parser.toString())
                // Starts parsing data
                var routes: List<List<HashMap<String, String>>> = parser.parse(jObject, applicationContext)
                Log.d("ParserTask", "Executing routes")
                Log.d("ParserTask", routes.toString())
                return routes
            } catch (e: Exception) {
                Log.d("ParserTask", e.toString())
                e.printStackTrace()
            }
            val r: List<List<HashMap<String, String>>> = ArrayList<ArrayList<HashMap<String, String>>>()
            return r
        }

        // Executes in UI thread, after the parsing process
        override fun onPostExecute(result: List<List<HashMap<String, String>>>) {
            var points: List<LatLng>
            var lineOptions: PolylineOptions? = null
            var wayPointBuilder: LatLngBounds.Builder? = null
            points = ArrayList<LatLng>()
            lineOptions = PolylineOptions()
            wayPointBuilder = LatLngBounds.builder()
            // Traversing through all the routes
            for (i in result.indices) {
                // Fetching i-th route
                val path = result[i]
                // Fetching all the points in i-th route
                for (j in path.indices) {
                    val point = path[j]
                    val lat = java.lang.Double.parseDouble(point["lat"])
                    val lng = java.lang.Double.parseDouble(point["lng"])
                    val position = LatLng(lat, lng)
                    points.add(position)
                }
                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points)
                /*      lineOptions.width(14f)
                      lineOptions.color(R.color.green)*/
                for (k in 0 until points.size) {
                    wayPointBuilder.include(points.get(k))
                }
                Log.d("onPostExecute", "onPostExecute lineoptions decoded")
            }
            var result_code: Int
            if (result.size != 0) {
                EventBus.getDefault().post(IntentServiceRouteResult(Activity.RESULT_OK, lineOptions, wayPointBuilder!!, points))
            } else {
                EventBus.getDefault().post(IntentServiceRouteResult(Activity.RESULT_CANCELED, lineOptions, wayPointBuilder!!, points))
                Log.d("onPostExecute", "without Polylines drawn")
            }
        }
    }






    //Fetch rerouting api call
    private fun fetchreroutingRouteUrl(url: String) {

        val responseCall = RetrofitInstance.getGoogleInstance().getRouteList(url)
        responseCall.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    if (response.body() != null) {
                        // mLoader?.dismiss()
                        try {
                            val responseStr = response.body()!!.string()
                            Log.e("getAddressListRes", responseStr)
                            val parserTask = ParserreroutingTask()
                            // Invokes the thread for parsing the JSON data
                            if (Build.VERSION.SDK_INT >= 11) {
                                parserTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,responseStr)
                            } else {
                                parserTask.execute(responseStr)
                            }

                        } catch (e: Exception) {
                            e.printStackTrace()
                        }

                    }
                }
                else
                {
                    Log.e("getAddressListRes", "Failed")
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                EventBus.getDefault().post(IntentServiceResult(Activity.RESULT_OK, "0", getString(R.string.mapfailedtointernet)))
            }
        })
    }
    @SuppressLint("StaticFieldLeak")
    private inner class ParserreroutingTask : AsyncTask<String, Int, List<List<HashMap<String, String>>>>() {

        // Parsing the data in non-ui thread
        override fun doInBackground(vararg jsonData: String): List<List<HashMap<String, String>>> {
            val jObject: JSONObject
            try {
                jObject = JSONObject(jsonData[0])
                Log.d("ParserTask", jsonData[0])
                val parser = RouteDataParser()
                Log.d("ParserTask", parser.toString())
                // Starts parsing data
                var routes: List<List<HashMap<String, String>>> = parser.parse(jObject, applicationContext)
                Log.d("ParserTask", "Executing routes")
                Log.d("ParserTask", routes.toString())
                return routes
            } catch (e: Exception) {
                Log.d("ParserTask", e.toString())
                e.printStackTrace()
            }
            val r: List<List<HashMap<String, String>>> = ArrayList<ArrayList<HashMap<String, String>>>()
            return r
        }

        // Executes in UI thread, after the parsing process
        override fun onPostExecute(result: List<List<HashMap<String, String>>>) {
            var points: List<LatLng>
            var lineOptions: PolylineOptions? = null
            var wayPointBuilder: LatLngBounds.Builder? = null
            points = ArrayList<LatLng>()
            lineOptions = PolylineOptions()
            wayPointBuilder = LatLngBounds.builder()
            // Traversing through all the routes
            for (i in result.indices) {
                // Fetching i-th route
                val path = result[i]
                // Fetching all the points in i-th route
                for (j in path.indices) {
                    val point = path[j]
                    val lat = java.lang.Double.parseDouble(point["lat"])
                    val lng = java.lang.Double.parseDouble(point["lng"])
                    val position = LatLng(lat, lng)
                    points.add(position)
                }
                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points)
                /*      lineOptions.width(14f)
                      lineOptions.color(R.color.green)*/
                for (k in 0 until points.size) {
                    wayPointBuilder.include(points.get(k))
                }
                Log.d("onPostExecute", "onPostExecute lineoptions decoded")
            }
            var result_code: Int
            if (result.size != 0) {
                EventBus.getDefault().post(IntentServiceReroutingRouteResult(Activity.RESULT_OK, lineOptions, wayPointBuilder!!, points))
            } else {
                EventBus.getDefault().post(IntentServiceReroutingRouteResult(Activity.RESULT_CANCELED, lineOptions, wayPointBuilder!!, points))
                Log.d("onPostExecute", "without Polylines drawn")
            }
        }
    }





























    // common api fetch service
    private fun commonapihit(header: HashMap<String, String>,api_name:String)
    {
        KotNetworking.post(urltohit)
              .addBodyParameter(header)
              .addHeaders(mSessionManager!!.getApiHeader())
              .setTag(this)
              .setPriority(Priority.HIGH)
              .build()
              .getAsString { response, error ->
              if (error != null)
              {
                  EventBus.getDefault().post(IntentServiceResult(Activity.RESULT_OK, responsetosend!!, api_name))
              }
              else
              {
                  // Success part
                  if (api_name.equals(getString(R.string.getappinfohit)))
                  {
                    saveauthkey(response.toString()!!)
                  }


                  else if (api_name.equals(getString(R.string.verifyvehiclenumber)))
                  {
                      val response_json_object = JSONObject(response)
                      try
                      {
                          val status = response_json_object.getString("status")
                          val response = response_json_object.getString("response")
                          val vehicleinfo = JSONObject(response)
                          val vehicleinfoobj = vehicleinfo.getString("vehicleinfo")
                          val vehicle_id = JSONObject(vehicleinfoobj)
                          val storevehicle_id = vehicle_id.getString("vehicle_id")


                          mSessionManager = SessionManager(applicationContext!!)
                          mSessionManager!!.settempvehicleid(storevehicle_id)
                          EventBus.getDefault().post(IntentServiceResult(Activity.RESULT_OK, status, getString(R.string.verifyvehiclenumber)))
                      }
                      catch (e: Exception)
                      {
                      }
                  }
                  else if (api_name.equals(getString(R.string.emailcheckk)))
                  {
                      try
                      {
                          val response_json_object = JSONObject(response.toString())
                          val status = response_json_object.getString("status")
                          if (status.equals("1"))
                          {
                              EventBus.getDefault().post(IntentServiceResult(Activity.RESULT_OK, status, getString(R.string.emailcheckk)))
                          }
                      }
                      catch (e: Exception)
                      {
                      }
                  }
                 else
                 {
                     EventBus.getDefault().post(IntentServiceResult(Activity.RESULT_OK, response.toString()!!, api_name))
                 }
              }
              }
    }

    // common api fetch service
    private fun dummycommonapihit(header: HashMap<String, String>)
    {
        KotNetworking.post(urltohit)
                .addBodyParameter(header)
                .addHeaders(mSessionManager!!.getApiHeader())
                .setTag(this)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsString { response, error ->
                    if (error != null)
                    {
                        // failure part
                        if (api_name.equals(getString(R.string.getmakemodelyear)))
                        {
                            EventBus.getDefault().post(IntentServiceResult(Activity.RESULT_OK, responsetosend!!, getString(R.string.getmakemodelyear)))
                        }
                        else if (api_name.equals(getString(R.string.getonlymake)))
                        {
                            EventBus.getDefault().post(IntentServiceResult(Activity.RESULT_OK, responsetosend!!, getString(R.string.getonlymake)))
                        }
                    }
                    else
                    {
                        // Success part
                        if (api_name.equals(getString(R.string.getmakemodelyear)))
                        {
                            EventBus.getDefault().post(IntentServiceResult(Activity.RESULT_OK, response.toString()!!, getString(R.string.getmakemodelyear)))
                        }
                        else if (api_name.equals(getString(R.string.getonlymake)))
                        {
                            savemakeandmodel(response.toString())
                            EventBus.getDefault().post(IntentServiceResult(Activity.RESULT_OK, response.toString()!!, getString(R.string.getonlymake)))
                        }
                    }
                }
    }

    // saving auth key to session
    fun saveauthkey(response:String)
    {
        try
        {
            val response_json_object = JSONObject(response)
            val status = response_json_object.getString("status")
            val response_object = `response_json_object`.getJSONObject("response")
            if (status.equals("1"))
            {
                //Xmpp Hostnames
                var xmpp_host_url = response_object.getString("xmpp_host_url")
                var xmpp_host_name = response_object.getString("xmpp_host_name")


                val app_identity_name = response_object.getString("app_identity_name")


                //Amazon bucket details
                var Str_s3_bucket_name = response_object.getString("s3_bucket")
                var Str_s3_access_key = response_object.getString("s3_access_key")
                var Str_s3_secret_key = response_object.getString("s3_secret_key")
                val data = Base64.decode(Str_s3_bucket_name, Base64.DEFAULT)
                Str_s3_bucket_name = String(data, Charset.forName("UTF-8"))
                val data1 = Base64.decode(Str_s3_access_key, Base64.DEFAULT)
                Str_s3_access_key = String(data1, Charset.forName("UTF-8"))
                val data2 = Base64.decode(Str_s3_secret_key, Base64.DEFAULT)
                Str_s3_secret_key = String(data2, Charset.forName("UTF-8"))




                mSessionManager!!.setS3Info(Str_s3_bucket_name, Str_s3_access_key, Str_s3_secret_key)
                mSessionManager = SessionManager(applicationContext!!)
                mSessionManager!!.storeauthkeyvalue(app_identity_name)
                mSessionManager!!.setXmppDetails(xmpp_host_url,xmpp_host_name)



                val upload_directories = response_object.getString("upload_directories")
                val response_json_object = JSONObject(upload_directories)

                val profile_picture = response_json_object.getString("profile_picture").replace("\\/","/")
                val vehicle_image = response_json_object.getString("vehicle_image").replace("\\/","/")
                val documentsFiles = response_json_object.getString("documents").replace("\\/","/")

                mSessionManager!!.setXmppFolderPath(profile_picture,vehicle_image,documentsFiles)
                EventBus.getDefault().post(IntentServiceResult(Activity.RESULT_OK, "1", getString(R.string.getappinfohit)))
            }
            else
            {
                EventBus.getDefault().post(IntentServiceResult(Activity.RESULT_OK, "0", getString(R.string.getappinfohit)))
            }
        }
        catch (e: Exception)
        {
        }
    }

    fun savemakeandmodel(response:String)
    {
        val response_json_object = JSONObject(response)
        try
        {
            val status = response_json_object.getString("status")
            if (status.equals("1"))
            {
                mSessionManager = SessionManager(applicationContext!!)
                mSessionManager!!.setCategoryDetails(response)
            }
        }
        catch (e: Exception)
        {
        }
    }
    fun getfcmtoken()
    {
        if(mSessionManager!!.getfcmkey().length < 5)
        {
            FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener { instanceIdResult ->
                val token = instanceIdResult.token
                saveFcmId((token))
            }
        }
    }

    private fun saveFcmId(token: String?)
    {
        var mSessionManager: SessionManager?
        mSessionManager = SessionManager(applicationContext)
        mSessionManager.setfcmkey(token!!)
    }

}