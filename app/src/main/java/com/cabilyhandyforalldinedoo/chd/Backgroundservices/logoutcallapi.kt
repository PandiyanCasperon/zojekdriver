package com.cabilyhandyforalldinedoo.chd.Backgroundservices


import android.annotation.SuppressLint
import android.app.Activity
import android.app.IntentService
import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.os.IBinder
import android.util.Log
import cabily.handyforall.dinedoo.R
import com.cabilyhandyforalldinedoo.chd.EventBusConnection.IntentServiceResult
import com.cabilyhandyforalldinedoo.chd.EventBusConnection.IntentServiceRouteResult
import com.cabilyhandyforalldinedoo.chd.SessionManagerPackage.SessionManager
import com.cabilyhandyforalldinedoo.chd.ViewModelRegisterModule.documetpagetwoViewModel
import com.cabilyhandyforalldinedoo.chd.ViewModelRideRequest.RouteDataParser
import com.cabilyhandyforalldinedoo.chd.data.duplicaterideid.duplicaterideidRecord
import com.cabilyhandyforalldinedoo.chd.data.duplicaterideid.duplicaterideidRepository
import com.cabilyhandyforalldinedoo.chd.data.steptwodocumentdb.documenttwoRepository
import com.cabilyhandyforalldinedoo.chd.data.steptwodocumentdb.documenttwodoRecord
import com.cabilyhandyforalldinedoo.chd.retrofit.RetrofitInstance
import com.cabilyhandyforalldinedoo.chd.ui.MainPage.riderequestpage
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.PolylineOptions
import com.mindorks.kotnetworking.KotNetworking
import com.mindorks.kotnetworking.common.Priority
import okhttp3.ResponseBody
import org.greenrobot.eventbus.EventBus
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


class logoutcallapi : IntentService("ApiHit")
{
    var header = HashMap<String, String>()
    var urltohit: String = ""
    lateinit private var mSessionManager: SessionManager

    override fun onBind(intent: Intent): IBinder?
    {
        throw UnsupportedOperationException("Not yet implemented")
    }

    override fun onHandleIntent(intent: Intent?)
    {
        mSessionManager = SessionManager(applicationContext!!)
        header.put("driver_id",mSessionManager!!.getDriverId())
        header.put("device","ANDROID")
        // url to hit
        urltohit = RetrofitInstance.logoutcall
        // common network call
        logoutcallhit(header,mSessionManager!!.getApiHeader())
        mSessionManager = SessionManager(applicationContext!!)
        mSessionManager!!.clearalldata()
        EventBus.getDefault().post(IntentServiceResult(Activity.RESULT_OK, "1", applicationContext.getString(R.string.logoutcallapi)))
    }



    // common api fetch service
    private fun logoutcallhit(header: HashMap<String, String>,headertag:HashMap<String, String>)
    {
        KotNetworking.post(urltohit)
                .addBodyParameter(header)
                .addHeaders(headertag)
                .setTag(this)
                .setPriority(Priority.HIGH)
                .build()
                .getAsString { response, error ->
                    if (error != null)
                    {
                        var response:String = response.toString()
                    }
                    else
                    {
                        var response:String = response.toString()
                    }
                }
    }


}