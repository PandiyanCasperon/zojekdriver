package com.cabilyhandyforalldinedoo.chd.Backgroundservices


import android.app.Activity
import android.app.IntentService
import android.content.Intent
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.IBinder
import cabily.handyforall.dinedoo.R
import com.cabilyhandyforalldinedoo.chd.EventBusConnection.IntentServiceResult
import com.cabilyhandyforalldinedoo.chd.SessionManagerPackage.SessionManager
import com.cabilyhandyforalldinedoo.chd.ViewModelTrackingPage.TrackingPageDataModel
import com.cabilyhandyforalldinedoo.chd.ViewModelTrackingPage.stopsModel
import com.cabilyhandyforalldinedoo.chd.clickableInterface.TripIntentServiceResult
import com.cabilyhandyforalldinedoo.chd.commonutils.CurrencySymbolConventer
import com.cabilyhandyforalldinedoo.chd.data.onridelatandlong.onrideDbHelper
import com.cabilyhandyforalldinedoo.chd.retrofit.RetrofitInstance
import com.mindorks.kotnetworking.KotNetworking
import com.mindorks.kotnetworking.common.Priority
import org.greenrobot.eventbus.EventBus
import org.json.JSONException
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*


class tripupdatecallapi : IntentService("ApiHit")
{

    var ridesttatus =""
    var responsetosend: String = "failed"
    var header = HashMap<String, String>()
    var urltohit: String = ""
    lateinit private var mSessionManager: SessionManager
    var useridtosend:String = ""
    var ride_id:String = ""

    override fun onBind(intent: Intent): IBinder?
    {
        throw UnsupportedOperationException("Not yet implemented")
    }

    override fun onHandleIntent(intent: Intent?)
    {

        mSessionManager = SessionManager(applicationContext!!)
         ride_id = mSessionManager.gerrideid()

         ridesttatus = mSessionManager.getdriverstatus()


        header.put("driver_id",mSessionManager!!.getDriverId())
        header.put("ride_id",ride_id)
        header.put("driver_lat", mSessionManager!!.getOnlineLatitiude())
        header.put("driver_lon", mSessionManager!!.getOnlineLongitude())

        var droplat = intent?.getStringExtra("droplat")
        var droplong = intent?.getStringExtra("droplong")

        useridtosend= intent!!.getStringExtra("useridtosend")

        if(ridesttatus.equals("1"))
        {
            // url to hit
            urltohit = RetrofitInstance.tripupdatearrived
        }
        else if(ridesttatus.equals("2"))
        {
            // url to hit
            urltohit = RetrofitInstance.begintripurl
        }
        else if(ridesttatus.equals("3"))
        {
            val travel_history: ArrayList<String>
            travel_history=getallrecordforparticularrideid(ride_id)
            val builder = StringBuilder()
            for (string in travel_history) {
                builder.append(",$string")
            }
            header.put("travel_history", builder.toString())
            // url to hit
            urltohit = RetrofitInstance.endtripurl
        }

        // common network call
        acknwoldegementcallhit(header)
    }

    // common api fetch service
    private fun acknwoldegementcallhit(header: HashMap<String, String>)
    {
        KotNetworking.post(urltohit)
                .addBodyParameter(header)
                .addHeaders(mSessionManager!!.getApiHeader())
                .setTag(this)
                .setPriority(Priority.HIGH)
                .build()
                .getAsString { response, error ->
                    if (error != null)
                    {
                        EventBus.getDefault().post(IntentServiceResult(Activity.RESULT_OK, responsetosend!!, getString(R.string.tripupdate)))
                    }
                    else
                    {
                        var response:String = response.toString()
                        responsetosend = response
                        try
                        {
                            var responseObj = JSONObject(response)
                            if (responseObj.getString("status").equals("1"))
                            {
                                if (responseObj.has("response"))
                                {
                                    var getingres: JSONObject? = responseObj.getJSONObject("response")
                                    var mSessionManager: SessionManager? = null
                                    mSessionManager = SessionManager(applicationContext!!)
                                    val list = `getingres`!!.getJSONArray("list")
                                    if (list.length() > 0)
                                    {
                                        for (i in 0 until list.length())
                                        {
                                            var j_ride_object = list.getJSONObject(i)
                                            var ridestatuss:String  = j_ride_object!!.getString("btn_group")
                                            mSessionManager .onridedetails(response,ridestatuss,"1")

                                            if(j_ride_object.has("user_token"))
                                            {
                                                var fcmtoken:String  = j_ride_object!!.getString("user_token")
                                                fcmsending(fcmtoken)
                                            }
                                            emittingthroughxmpp()
                                        }
                                    }
                                       }
                            }
                            else if (responseObj.getString("status").equals("0"))
                            {
                                EventBus.getDefault().post(IntentServiceResult(Activity.RESULT_OK, responsetosend!!, "canceltri"))

                            }
                        }
                        catch (e: JSONException)
                        {
                        }

                    }
                }
    }




    fun fcmsending(fcmtoken:String)
    {
        val registerpagetwopage = Intent(applicationContext, fcmsendingprocess::class.java)
        registerpagetwopage.putExtra("rideid", ride_id)
        registerpagetwopage.putExtra("ridestatus", "")
        registerpagetwopage.putExtra("useridtosend", useridtosend)
        if(ridesttatus.equals("1"))
        {
            registerpagetwopage.putExtra("actions", "cab_arrived")
        }
        else if(ridesttatus.equals("2"))
        {
            registerpagetwopage.putExtra("actions", "trip_begin")
        }
        else if(ridesttatus.equals("3"))
        {
            registerpagetwopage.putExtra("actions", "make_payment")
        }
        registerpagetwopage.putExtra("messages", "")
        registerpagetwopage.putExtra("fcmtoken", fcmtoken)
        startService(registerpagetwopage)
    }

    fun getallrecordforparticularrideid(ride_id:String): ArrayList<String>
    {
        val endDataTrips = ArrayList<String>()
        var mHelper: onrideDbHelper? = null
        var dataBase: SQLiteDatabase? = null
        mHelper= onrideDbHelper(applicationContext)
        dataBase = mHelper!!.getWritableDatabase();
        var mCursor: Cursor = dataBase!!.rawQuery("SELECT * FROM " + onrideDbHelper.TABLE_NAME+" WHERE rideid='"+ride_id+"' ORDER BY id ASC", null);
        if (mCursor.moveToFirst())
        {
            do
            {
                var latitude= mCursor.getString(mCursor.getColumnIndex(onrideDbHelper.latitude))
                var longitude= mCursor.getString(mCursor.getColumnIndex(onrideDbHelper.longitude))
                var time= mCursor.getString(mCursor.getColumnIndex(onrideDbHelper.time))
                endDataTrips.add(latitude + ";" + longitude + ";" + time)
            } while (mCursor.moveToNext());
        }
        mCursor.close()
        val aCalendar = Calendar.getInstance()
        val aDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val aCurrentDate = aDateFormat.format(aCalendar.getTime())
        endDataTrips.add(mSessionManager!!.getTrackingLat() + ";" + mSessionManager!!.getTrackingLong() + ";" + aCurrentDate)
        return endDataTrips
    }


    fun emittingthroughxmpp()
    {
        var actions = ""
        if(ridesttatus.equals("1"))
            actions = "cab_arrived"
        else if(ridesttatus.equals("2"))
            actions ="trip_begin"
        else if(ridesttatus.equals("3"))
            actions ="make_payment"

        val job = JSONObject()
        job.put("action", actions)
        job.put("rideid", ride_id)
        EventBus.getDefault().post(TripIntentServiceResult(job.toString(),useridtosend+"@"+mSessionManager!!.getxmpp_host_name()))
        EventBus.getDefault().post(IntentServiceResult(Activity.RESULT_OK, responsetosend!!, getString(R.string.tripupdate)))
    }
}