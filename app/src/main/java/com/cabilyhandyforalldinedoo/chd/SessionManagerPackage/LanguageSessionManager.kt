package com.cabilyhandyforalldinedoo.chd.SessionManagerPackage

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.util.Log

class LanguageSessionManager(var mContext: Context)
{
    var mSharedPreferenceName = "languagefile"
    var mSharedPreferenceMode = 0
    private var mSharedPreference: SharedPreferences = mContext.getSharedPreferences(mSharedPreferenceName, mSharedPreferenceMode)
    private var mEditor = mSharedPreference.edit()

    val languagechoosen = "languagechoosen"



    fun setLanguageChoosen(value:String)
    {
        mEditor.putString(languagechoosen, value)
        mEditor.apply()
    }

    fun getlanguageoption(): String {
        return mSharedPreference.getString(languagechoosen, "2")
    }


}
