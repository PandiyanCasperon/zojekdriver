package com.cabilyhandyforalldinedoo.chd.SessionManagerPackage

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.util.Log

class SessionManager(var mContext: Context)
{
    var mSharedPreferenceName = "cabilyhandy"
    var mSharedPreferenceMode = 0
    private var mSharedPreference: SharedPreferences = mContext.getSharedPreferences(mSharedPreferenceName, mSharedPreferenceMode)
    private var mEditor = mSharedPreference.edit()

    // flag & country code saving
    val flagsaved = "flagsaved"
    val countrycodesaved = "countrycodesaved"
    val countrynamesaved = "countrynamesaved"

    // fcm key
    val fcmKey = "fcmkey"

    val onlineavailable = "onlineavailable"

    // online car update
    val onlinelatitude = "onlinelatitude"
    val onlinelongitude = "onlinelongitude"
    val onlinebearing = "onlinebearing"
    val onlineride_id = "onlineride_id"

    // onride page
    val onridelat = "onridelat"
    val onridelong = "onridelong"
    val onridebearing = "onridebearing"
    val onridestatsu = "onridestatsu"
    val onridecount = "onridecount"

    // step 1
    val setlocationid = "locationid"
    val setlocationname = "locationname"

    val setcategoryid = "setcategoryid"
    val setcategoryname = "setcategoryname"

    val currencysymbol = "currencysymbol"

    val setlocationmodee = "setlocationmodee"

    val tempvehicleid = "tempvehicleid"


    val setvehicletypeid = "setvehicletypeid"
    val setvehicletypename = "setvehicletypename"

    val setmakeid = "setmakeid"
    val setmakename = "setmakename"

    val setmodelid = "setmodelid"
    val setmmodelname = "setmmodelname"
    val setvehicleoneid = "setvehicleoneid"

    val setvehicleimage = "setvehicleimage"

    val setyear = "setyear"
    val setveehiclenumber = "setveehiclenumber"

    val stepstoselect = "stepstoselect"

    private val distance_km: String = "distance_km"
    private val distance_meter: String = "distance_meter"
    private val duration_hours: String = "duration_hours"
    private val duration_secs: String = "duration_secs"

    private val savehittedrideid: String = "savehittedrideid"

    val catkeyres = "catkey"

    // profile details
    val hasSession = "hasSession"
    val id = "id"
    val first_name = "first_name"
    val last_name = "last_name"
    val driver_name = "driver_name"

    //s3 bucket
    val KEY_S3_BUCKET_NAME = "bucket_name"
    val KEY_S3_ACCESS_KEY = "access_key"
    val KEY_S3_SECRET_KEY = "secret_key"

    //Xmpp Data
    val XMPPHOSTURL = "xmpp_host_url"
    val XMPPHOSTNAME = "xmpp_host_name"

    //Xmpp Data
    val XMPPprofile_picture = "xmppprofile_picture"
    val XMPPvehicle_image = "xmpppvehicle_image"
    val XMPPdocuments = "xmpppdocuments"

    //on ride
    val fulljson = "fulljson"
    val ridehasSession = "ridehasSession"
    val ridestatus = "ridestatus"

    //settindpage
    val reroutingopetionn = "reroutingopetionn"
    val nightmodeotpion = "nightmodeotpion"
    val navigateoptionn = "navigateoptionn"


    var regis_temp_driver_id = "regis_temp_driver_id"

    val storeauthkey = "storeauthkey"

    val ridepickuplatitude = "ridepickuplatitude"
    val ridepickuplongitude = "ridepickuplongitude"
    val ridedroplatitiude = "ridedroplatitiude"
    val ridedroplongitude = "ridedroplongitude"
    val rideid = "rideid"
    val whreyougot = "whreyougot"

    val documentpending = "documentpending"

    val mobilecode = "mobilecode"
    val mobilenumber = "mobilenumber"
    val hasSessiontomainpage = "hasSessiontomainpage"



    val apartment = "apartment"
    val address = "address"
    val countryid = "countryid"
    val state = "state"
    val driverreview = "driverreview"
    val city = "city"
    val servicelocationid = "servicelocationid"
    val zipcode = "zipcode"

    val drivermobileno = "drivermobileno"
    val fcmtokenusers = "fcmtokenusers"
    val dutyrideid = "dutyrideid"

    val email = "email"
    val gender = "gender"
    val dob = "dob"
    val driver_image = "driver_image"
    val stage = "stage"

    fun setFlagDetails(flag: String, countrycode: String, countryname: String)
    {
        mEditor.putString(flagsaved, flag)
        mEditor.putString(countrycodesaved, countrycode)
        mEditor.putString(countrynamesaved, countryname)
        mEditor.commit()
        mEditor.apply()
    }
    fun getFlagDetails(): HashMap<String, String>
    {
        var flagdetails = HashMap<String, String>()
        flagdetails[flagsaved]        = mSharedPreference.getString(flagsaved, "0")
        flagdetails[countrycodesaved] = mSharedPreference.getString(countrycodesaved, "0")
        flagdetails[countrynamesaved]      = mSharedPreference.getString(countrynamesaved, "0")
        return flagdetails
    }
    fun setfcmkey(fcmkeys: String)
    {
        mEditor.putString(this.fcmKey, fcmkeys)
        mEditor.apply()
    }
    fun getfcmkey(): String
    {
        return mSharedPreference.getString(fcmKey, "")
    }

    fun setTravellingDistanceDuration(distance_km: String, distance_meter: String, duration_hours: String, duration_secs: String) {
        mEditor.putString(this.distance_km, distance_km)
        mEditor.putString(this.distance_meter, distance_meter)
        mEditor.putString(this.duration_hours, duration_hours)
        mEditor.putString(this.duration_secs, duration_secs)
        mEditor.apply()
    }

    fun storeauthkeyvalue(authkey: String) {
        mEditor.putString(this.storeauthkey, authkey)
        mEditor.apply()
    }

    fun getStoreauthKey(): String {
        return mSharedPreference.getString(storeauthkey, "")
    }


    fun getTravellingDistanceMeter(): String {
        return mSharedPreference.getString(distance_meter, "")
    }

    fun getTravellingDurationSecs(): String {
        return mSharedPreference.getString(duration_secs, "")
    }



    fun gethittedrideid(): String {
        return mSharedPreference.getString(savehittedrideid, "")
    }
    fun savehittedrideidvalue(rideid: String)
    {
        mEditor.putString(savehittedrideid, rideid)
        mEditor.commit()
    }

    fun setS3Info(str_s3_bucket_name: String, str_s3_access_key: String, str_s3_secret_key: String)
    {
        mEditor.putString(KEY_S3_BUCKET_NAME, str_s3_bucket_name)
        mEditor.putString(KEY_S3_ACCESS_KEY, str_s3_access_key)
        mEditor.putString(KEY_S3_SECRET_KEY, str_s3_secret_key)
        mEditor.commit()
    }

    fun setXmppDetails(xmpp_host_url: String, xmpp_host_name: String)
    {
        mEditor.putString(XMPPHOSTURL, xmpp_host_url)
        mEditor.putString(XMPPHOSTNAME, xmpp_host_name)
        mEditor.commit()
    }

    fun setXmppFolderPath(profile_picture: String, vehicle_image: String, documents: String)
    {
        mEditor.putString(XMPPprofile_picture, profile_picture)
        mEditor.putString(XMPPvehicle_image, vehicle_image)
        mEditor.putString(XMPPdocuments, documents)
        mEditor.commit()
    }

    fun getxmppprofile_picture(): String? {
        return mSharedPreference.getString(XMPPprofile_picture, "")
    }

    fun getxmppvehicle_image(): String? {
        return mSharedPreference.getString(XMPPvehicle_image, "")
    }

    fun getxmppdocuments(): String? {
        return mSharedPreference.getString(XMPPdocuments, "")
    }


    fun getxmpp_host_url(): String? {
        return mSharedPreference.getString(XMPPHOSTURL, "")
    }

    fun getxmpp_host_name(): String? {
        return mSharedPreference.getString(XMPPHOSTNAME, "")
    }



    fun getKeyS3BucketName(): String? {
        return mSharedPreference.getString(KEY_S3_BUCKET_NAME, "")

    }

    fun getKeyS3AccessKey(): String? {
        return mSharedPreference.getString(KEY_S3_ACCESS_KEY, "")

    }

    fun getKeyS3SecretKey(): String? {
        return mSharedPreference.getString(KEY_S3_SECRET_KEY, "")

    }



    fun getApiHeader(): HashMap<String, String>
    {
        var header = HashMap<String, String>()
        header.put("apptoken", mSharedPreference.getString(fcmKey, ""))
        header.put("Authkey", getStoreauthKey())
        header.put("isapplication", "1")
        header.put("applanguage", "en")
        header.put("apptype", "android")
        header.put("driverid", getDriverId())
        return header
    }
    fun createLoginSession(idvalue: String, first_namevalue: String, last_namevalue: String,
                           driver_namevalue: String, emailvalue: String, gendervalue: String,
                           dobvalue: String, driver_imagevalue: String, stagevalue: String, mobilecodesend: String, mobilenumbersend: String, addresssend: String, countryidsend: String, statesend: String, citysend: String, servicelocationsend: String, zipcodesend: String)
    {
        mEditor.putString(id, idvalue)
        mEditor.putString(first_name, first_namevalue)
        mEditor.putString(last_name, last_namevalue)
        mEditor.putString(driver_name, driver_namevalue)
        mEditor.putString(email, emailvalue)
        mEditor.putString(gender, gendervalue)
        mEditor.putString(dob, dobvalue)
        mEditor.putString(driver_image, driver_imagevalue)
        mEditor.putString(stage, stagevalue)
        mEditor.putString(mobilecode, mobilecodesend)
        mEditor.putString(mobilenumber, mobilenumbersend)

        mEditor.putString(driverreview, "0")


        mEditor.putString(address, addresssend)
        mEditor.putString(countryid, countryidsend)
        mEditor.putString(state, statesend)
        mEditor.putString(city, citysend)
        mEditor.putString(servicelocationid, servicelocationsend)
        mEditor.putString(zipcode, zipcodesend)

        mEditor.putBoolean(hasSession, false)
        mEditor.apply()
    }

    fun driverProfileData(driver_id: String, driver_names: String, driver_imagevalue: String, emailvalue: String, mobilecodesend: String, mobilenumbersend: String)
    {
        mEditor.putString(id, driver_id)
        mEditor.putString(driver_name, driver_names)
        mEditor.putString(driver_image, driver_imagevalue)
        mEditor.putString(email, emailvalue)

        mEditor.putString(mobilecode, mobilecodesend)
        mEditor.putString(mobilenumber, mobilenumbersend)

        mEditor.apply()
    }


    fun updatemobileno(code: String, mobileno: String)
    {
        mEditor.putString(mobilecode, code)
        mEditor.putString(mobilenumber, mobileno)
        mEditor.apply()
    }

    fun updateemailalone(emailx: String)
    {
        mEditor.putString(email, emailx)
        mEditor.apply()
    }







    fun onridedetails(fulljsossn: String,status:String,whreyougotvalue:String)
    {
        mEditor.putString(fulljson, fulljsossn)
        mEditor.putString(ridestatus,status)
        mEditor.putString(ridepickuplatitude, "")
        mEditor.putString(ridepickuplongitude, "")
        mEditor.putString(ridedroplatitiude, "")
        mEditor.putString(ridedroplongitude, "")
        mEditor.putString(rideid, "")
        mEditor.putString(whreyougot,whreyougotvalue)
        mEditor.putBoolean(ridehasSession, true)
        mEditor.apply()
    }


    fun getWhereyougot(): String {
        return mSharedPreference.getString(whreyougot, "")
    }

    fun setUsertripfcmtoken(fcmtokenuser: String)
    {
        mEditor.putString(this.fcmtokenusers, fcmtokenuser)
        mEditor.apply()
    }

    fun setDutyrideid(dutyrideids: String)
    {
        mEditor.putString(this.dutyrideid, dutyrideids)
        mEditor.apply()
    }


    fun getdutyrideid(): String {
        return mSharedPreference.getString(dutyrideid, "")
    }

    fun getUserfcmtoken(): String {
        return mSharedPreference.getString(fcmtokenusers, "")
    }

    fun setDriverMobileNO(mobno: String)
    {
        mEditor.putString(this.drivermobileno, mobno)
        mEditor.apply()
    }


    fun getDriverMobileNo(): String {
        return mSharedPreference.getString(drivermobileno, "")
    }

    fun setRidepickuplatitude(ridepickuplatitude: String)
    {
        mEditor.putString(this.ridepickuplatitude, ridepickuplatitude)
        mEditor.apply()
    }

    fun setservicelocation(locaid: String)
    {
        mEditor.putString(this.servicelocationid, locaid)
        mEditor.apply()
    }


    fun getridepickuplatitude(): String {
        return mSharedPreference.getString(ridepickuplatitude, "")
    }
    fun getrideridepickuplongitude(): String {
        return mSharedPreference.getString(ridepickuplongitude, "")
    }


    fun setRidepickuplongitude(ridepickuplongitude: String)
    {
        mEditor.putString(this.ridepickuplongitude, ridepickuplongitude)
        mEditor.apply()
    }
    fun setRidedroplatitiude(ridedroplatitiude: String)
    {
        mEditor.putString(this.ridedroplatitiude, ridedroplatitiude)
        mEditor.apply()
    }
    fun setRidedroplongitude(ridedroplongitude: String)
    {
        mEditor.putString(this.ridedroplongitude, ridedroplongitude)
        mEditor.apply()
    }

    fun setRideid(rideidd: String)
    {
        mEditor.putString(this.rideid, rideidd)
        mEditor.apply()
    }

    fun gerrideid(): String {
        return mSharedPreference.getString(rideid, "")
    }


    fun setDocPending(docstatus: String)
    {
        mEditor.putString(this.documentpending, docstatus)
        mEditor.apply()
    }

    fun getDocPending(): String {
        return mSharedPreference.getString(documentpending, "")
    }



    fun setReview(review: String)
    {
        mEditor.putString(this.driverreview, review)
        mEditor.apply()
    }

    fun getReview(): String {
        return mSharedPreference.getString(driverreview, "0")
    }


    fun clearridedetails()
    {
        mEditor.putString(fulljson, "")
        mEditor.putString(ridestatus, "")
        mEditor.putString(ridepickuplatitude, "")
        mEditor.putString(ridepickuplongitude, "")
        mEditor.putString(ridedroplatitiude, "")
        mEditor.putString(ridedroplongitude, "")
        mEditor.putBoolean(ridehasSession, false)
        mEditor.apply()
    }


    fun settridestatusforcash()
    {
        mEditor.putString(ridestatus, "8")
        mEditor.apply()
    }

    fun setreroutingoption(isChecked:Boolean)
    {
        mEditor.putBoolean(reroutingopetionn, isChecked)
        mEditor.apply()
    }

    fun setnightmodeoption(isChecked:Boolean)
    {
        mEditor.putBoolean(nightmodeotpion, isChecked)
        mEditor.apply()
    }

    fun setNavigationOption(value:String)
    {
        mEditor.putString(navigateoptionn, value)
        mEditor.apply()
    }


    fun getnavigateopition(): String {
        return mSharedPreference.getString(navigateoptionn, "1")
    }

    fun getreoutesettingboolean(): Boolean {
        return mSharedPreference.getBoolean(reroutingopetionn, false)
    }

    fun getnightmodeboolean(): Boolean {
        return mSharedPreference.getBoolean(nightmodeotpion, false)
    }

    fun getdriverstatus(): String {
        return mSharedPreference.getString(ridestatus, "")
    }

    fun ridehasSession(): Boolean {
        return mSharedPreference.getBoolean(ridehasSession, false)
    }

    fun gettripdetails(): String {
        return mSharedPreference.getString(fulljson, "")
    }

    fun hasSession(): Boolean {
        return mSharedPreference.getBoolean(hasSessiontomainpage, false)
    }


    fun setHasseesion(hasSessionis: Boolean) {
        mEditor.putBoolean(this.hasSessiontomainpage, hasSessionis)
        mEditor.apply()
    }



    fun getUserDetails(): HashMap<String, String>
    {
        var userDetails = HashMap<String, String>()
        userDetails[id] = mSharedPreference.getString(id, "")
        userDetails[first_name] = mSharedPreference.getString(first_name, "")
        userDetails[last_name] = mSharedPreference.getString(last_name, "")
        userDetails[driver_name] = mSharedPreference.getString(driver_name, "")
        userDetails[driver_image] = mSharedPreference.getString(driver_image, "")
        userDetails[email] = mSharedPreference.getString(email, "")

        userDetails[gender] = mSharedPreference.getString(gender, "")
        userDetails[dob] = mSharedPreference.getString(dob, "")
        userDetails[stage] = mSharedPreference.getString(stage, "")
        userDetails[mobilecode] = mSharedPreference.getString(mobilecode, "")
        userDetails[mobilenumber] = mSharedPreference.getString(mobilenumber, "")

        userDetails[apartment] = mSharedPreference.getString(apartment, "")
        userDetails[address] = mSharedPreference.getString(address, "")
        userDetails[countryid] = mSharedPreference.getString(countryid, "")
        userDetails[state] = mSharedPreference.getString(state, "")
        userDetails[city] = mSharedPreference.getString(city, "")
        userDetails[servicelocationid] = mSharedPreference.getString(servicelocationid, "")
        userDetails[zipcode] = mSharedPreference.getString(zipcode, "")







        return userDetails
    }

    fun createLoginSuccess()
    {
        mEditor.putBoolean(hasSession, true)
        mEditor.apply()
    }


    fun settempid( temp_driver_id: String)
    {
        mEditor.putString(regis_temp_driver_id, temp_driver_id)
        mEditor.commit()
    }

    fun gettempdriverID(): String
    {
        return mSharedPreference.getString(regis_temp_driver_id, "")
    }


    fun getDriverId(): String
    {
        return mSharedPreference.getString(id, "")
    }

    fun getLoginSuccess(): Boolean
    {
        return mSharedPreference.getBoolean(hasSession, false)
    }
    fun setCategoryDetails(catrespoanse: String)
    {
        mEditor.putString(this.catkeyres, catrespoanse)
        mEditor.apply()
    }
    fun getCategoryDetails(): String
    {
        return mSharedPreference.getString(catkeyres, "")
    }



    fun setOnlineAvailability(onlinestatsu: String)
    {
        mEditor.putString(this.onlineavailable, onlinestatsu)
        mEditor.apply()
    }
    fun getOnlineAvailability(): String
    {
        return mSharedPreference.getString(onlineavailable, "No")
    }



    fun setOnlineStatsu(onlinelatitude: Double,onlinelongitude:Double,onlinebearing:Float,onlineride_id:String)
    {
        mEditor.putString(this.onlinelatitude,  onlinelatitude.toString())
        mEditor.putString(this.onlinelongitude, onlinelongitude.toString())
        mEditor.putString(this.onlinebearing,   onlinebearing.toString())
        mEditor.putString(this.onlineride_id,   onlineride_id)
        mEditor.apply()
    }


    fun setTrackinPagesession(onlinelatitude: Double,onlinelongitude:Double,onlinebearing:Float,onlineride_id:String,ridecount:String)
    {
        mEditor.putString(this.onridelat,  onlinelatitude.toString())
        mEditor.putString(this.onridelong, onlinelongitude.toString())
        mEditor.putString(this.onridebearing,   onlinebearing.toString())
        mEditor.putString(this.onridestatsu,   onlineride_id)
        mEditor.putString(this.onridecount,   ridecount)
        mEditor.apply()
    }

    fun getOnlineLatitiude(): String
    {
        return mSharedPreference.getString(onlinelatitude, "0")
    }
    fun getOnlineLongitude(): String
    {
        return mSharedPreference.getString(onlinelongitude, "0")
    }
    fun getOnlineBEaring(): String
    {
        return mSharedPreference.getString(onlinebearing, "0")
    }
    fun getOnrideCount(): String
    {
        return mSharedPreference.getString(onridecount, "0")
    }


    fun getTrackingLat(): String
    {
        return mSharedPreference.getString(onridelat, "0.0")
    }
    fun getTrackingLong(): String
    {
        return mSharedPreference.getString(onridelong, "0.0")
    }
    fun getbearing(): String
    {
        return mSharedPreference.getString(onridebearing, "0.0")
    }


    fun setLocation(locationid: String,locationname:String)
    {
        mEditor.putString(this.setlocationid, locationid)
        mEditor.putString(this.setlocationname, locationname)
        mEditor.apply()
    }
    fun clearlocation()
    {
        mEditor.putString(this.setlocationid, "")
        mEditor.putString(this.setlocationname, "")
        mEditor.apply()
    }
    fun getLocationID(): String
    {
        return mSharedPreference.getString(setlocationid, "")
    }
    fun getLocationName(): String
    {
        return mSharedPreference.getString(setlocationname, "")
    }















    fun setCategory(locationid: String,locationname:String)
    {
        mEditor.putString(this.setcategoryid, locationid)
        mEditor.putString(this.setcategoryname, locationname)
        mEditor.apply()
    }
    fun clearCategory()
    {
        mEditor.putString(this.setcategoryid, "")
        mEditor.putString(this.setcategoryname, "")
        mEditor.apply()
    }
    fun getCategoryID(): String
    {
        return mSharedPreference.getString(setcategoryid, "")
    }
    fun geCategoryName(): String
    {
        return mSharedPreference.getString(setcategoryname, "")
    }

    fun setCategoryNamealone(locationname:String)
    {
        mEditor.putString(this.setcategoryname, locationname)
        mEditor.apply()
    }




    fun setcurrencysymbol(symbol:String)
    {
        mEditor.putString(this.currencysymbol, symbol)
        mEditor.apply()
    }

    fun getCurrencySymbol(): String
    {
        return mSharedPreference.getString(currencysymbol, "")
    }


    fun setlocationmode(setmode:String)
    {
        mEditor.putString(this.setlocationmodee, setmode)
        mEditor.apply()
    }

    fun settempvehicleid(tempid:String)
    {
        mEditor.putString(this.tempvehicleid, tempid)
        mEditor.apply()
    }

    fun cleatempvehicleid()
    {
        mEditor.putString(this.tempvehicleid, "")
        mEditor.apply()
    }


    fun gettempvehicleid(): String
    {
        return mSharedPreference.getString(tempvehicleid, "")
    }

    fun getLocationMode(): String
    {
        return mSharedPreference.getString(setlocationmodee, "0")
    }


    fun setVehicleType(VehicleTypeid: String,VehicleTypename:String)
    {
        mEditor.putString(this.setvehicletypeid, VehicleTypeid)
        mEditor.putString(this.setvehicletypename, VehicleTypename)
        mEditor.apply()
    }
    fun clearVehicleType()
    {
        mEditor.putString(this.setvehicletypeid, "")
        mEditor.putString(this.setvehicletypename, "")
        mEditor.apply()
    }
    fun getVehicleTypeID(): String
    {
        return mSharedPreference.getString(setvehicletypeid, "")
    }
    fun getVehicleTypeName(): String
    {
        return mSharedPreference.getString(setvehicletypename, "")
    }





    fun setVehicleImage(setvehicleimages: String)
    {
        mEditor.putString(this.setvehicleimage, setvehicleimages)
        mEditor.apply()
    }

    fun clearvehicleimage()
    {
        mEditor.putString(this.setvehicleimage, "")
        mEditor.apply()
    }

    fun getVehicleImage(): String
    {
        return mSharedPreference.getString(setvehicleimage, "")
    }





    fun setMake(makeid: String,makename:String)
    {
        mEditor.putString(this.setmakeid, makeid)
        mEditor.putString(this.setmakename, makename)
        mEditor.apply()
    }
    fun clearMake()
    {
        mEditor.putString(this.setmakeid, "")
        mEditor.putString(this.setmakename, "")
        mEditor.apply()
    }
    fun getMakeID(): String
    {
        return mSharedPreference.getString(setmakeid, "")
    }
    fun getMakeName(): String
    {
        return mSharedPreference.getString(setmakename, "")
    }
















    fun setVehicleStepOneId(vehicleoneid: String)
    {
        mEditor.putString(this.setvehicleoneid, vehicleoneid)
        mEditor.apply()
    }

    fun getVehicleStepOneId(): String
    {
        return mSharedPreference.getString(setvehicleoneid, "")
    }

    fun setModelnamealone(Modelname: String)
    {
        mEditor.putString(this.setmmodelname, Modelname)
        mEditor.apply()
    }

    fun setModel(Modelid: String,Modelname:String)
    {
        mEditor.putString(this.setmodelid, Modelid)
        mEditor.putString(this.setmmodelname, Modelname)
        mEditor.apply()
    }
    fun clearModel()
    {
        mEditor.putString(this.setmodelid, "")
        mEditor.putString(this.setmmodelname, "")
        mEditor.apply()
    }
    fun getModelID(): String
    {
        return mSharedPreference.getString(setmodelid, "")
    }
    fun getModelName(): String
    {
        return mSharedPreference.getString(setmmodelname, "")
    }



















    fun setYear(year: String)
    {
        mEditor.putString(this.setyear, year)
        mEditor.apply()
    }
    fun clearYear()
    {
        mEditor.putString(this.setyear, "")
        mEditor.apply()
    }
    fun getYearID(): String
    {
        return mSharedPreference.getString(setyear, "")
    }



    fun setvehiclenumber(vehiclenumber: String)
    {
        mEditor.putString(this.setveehiclenumber, vehiclenumber)
        mEditor.apply()
    }

    fun clearvehiclenumber()
    {
        mEditor.putString(this.setveehiclenumber, "")
        mEditor.apply()
    }

    fun getVehicleno(): String
    {
        return mSharedPreference.getString(setveehiclenumber, "")
    }









    fun clearalldata()
    {
        mEditor.clear().commit()
    }






    fun setSteps(steps: String)
    {
        mEditor.putString(this.stepstoselect, steps)
        mEditor.apply()
    }
    fun getSteps(): String
    {
        return mSharedPreference.getString(stepstoselect, "")
    }



}
